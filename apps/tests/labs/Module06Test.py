from labs.module06.MqttClientConnector import MqttClientConnector
from labs.common.SensorData import SensorData
from labs.common.ActuatorData import ActuatorData
from paho.mqtt.client import MQTTMessage

import unittest

"""
Test class for all requisite Module06 functionality.
"""

class Module06Test(unittest.TestCase):

	"""
		Sets up the Initial Configuration for testing
	"""

	def setUp(self):
		self.mqttClient = MqttClientConnector()
		self.mqttClient.setupMqttClient()

	"""
		Tears down the dummy objects set before
	"""

	def tearDown(self):
		del self.mqttClient

	"""
		Tests publishing the SensorData.
	"""

	def testPublishingSensorData(self):
		sensorData = SensorData()
		sensorData.setName("Temperature")
		sensorData.addValue(20)
		publishStatus = self.mqttClient.publishSensorData(sensorData, 0)
		self.assertTrue(publishStatus, "The Publish Process wasn't successful!")
	
	"""
		Tests publishing the ActuatorData.
	"""	

	def testPublishingActuatorData(self):
		actuatorData = ActuatorData()
		actuatorData.setCommand("TOO HOT")
		actuatorData.setName("TEMP")
		actuatorData.setValue(20)
		publishStatus = self.mqttClient.publishActuatorCommand(actuatorData, 0)
		self.assertTrue(publishStatus, "The publish process was not successful!")
	
	'''
		Tests successful subscription of SensorData
	'''

	def testSubscribingSensorData(self):
		mqttMessage = MQTTMessage()
		mqttMessage.topic = "topic/test".encode("utf-8")
		mqttMessage.payload = "{\"timeStamp\": \"2020-02-20 00:26:41.758011\", \"sampleCount\": 1, \"curValue\": 20, \"totValue\": 20, \"minValue\": 20, \"maxValue\": 20, \"avgValue\": 20.0, \"name\": \"Temperature\"}".encode("utf-8")
		receiveStatus = self.mqttClient.messageReceived(mqttMessage)
		self.assertTrue(receiveStatus, "Message received isn't recognized or processed unsuccessfully!")
	
	'''
		Tests successful subscription of ActuatorData
	'''	

	def testSubsribingActuatorData(self):
		mqttMessage = MQTTMessage()
		mqttMessage.topic = "topic/actuatorDataTest".encode("utf-8")
		mqttMessage.payload = "{\"command\": \"HOT TEMP\", \"value\": 20, \"name\": \"TEMP\"}".encode("utf-8")
		receiveStatus = self.mqttClient.messageReceived(mqttMessage)
		self.assertTrue(receiveStatus, "Message received isn't recognized or processed unsuccessfully!")
	
		
if __name__ == "__main__":
	# import sys;sys.argv = ['', 'Test.testName']
	unittest.main()
