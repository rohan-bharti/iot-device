import unittest
from labs.module01.systemMemUtilTask import SystemMemUtilTask
from labs.module01.systemCpuUtilTask import SystemCpuUtilTask

"""
Test class for all requisite Module01 functionality.
"""
class Module01Test(unittest.TestCase):
	
	"""
	Fetches the Cpu Memory Usage and checks if it is a float value between 0.0 and 100.0 or not
	"""
	def testMemoryUsageFetch(self):
		systemMemUtilTask = SystemMemUtilTask()
		cpuMem = systemMemUtilTask.getDataFromSensor()
		if 0.0 <= cpuMem <= 100.0:
			pass
		else:
			self.fail('The value is not a float value between 0 and 100')

	"""
	Fetches the Cpu Util Percentage and checks if it is a float value between 0.0 and 100.0 or not
	"""
	def testCpuUtilPercentageFetch(self):
		systemCpuUtilTask = SystemCpuUtilTask()
		cpuUtil = systemCpuUtilTask.getDataFromSensor()
		if 0.0 <= cpuUtil <= 100.0:
			pass
		else:
			self.fail('The value is not a float value between 0 and 100')
			
if __name__ == "__main__":
	unittest.main()