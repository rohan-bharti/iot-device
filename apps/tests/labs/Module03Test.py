from labs.module03.TempSensorAdaptorTask import TempSensorAdaptorTask
from labs.module03.TempActuatorAdaptor import TempActuatorAdaptor
from labs.common.ActuatorData import ActuatorData
from labs.module03.SensorDataManager import SensorDataManager
from labs.common.SensorData import SensorData

import unittest

"""
	Tests all the classes in the Module 3 package
"""


class Module03Test(unittest.TestCase):

	"""
		Sets up the Initial Configuration for testing
	"""

	def setUp(self):
		self.sensorDataManager = SensorDataManager()
		self.tempSensorAdaptorTask = TempSensorAdaptorTask(self.sensorDataManager)
		self.tempActuatorAdaptor = TempActuatorAdaptor()

	"""
		Tears down the dummy objects set before
	"""

	def tearDown(self):
		del self.sensorDataManager
		del self.tempActuatorAdaptor
		del self.tempSensorAdaptorTask

	"""
		Test is the SenseHat is supplying temperature values or not
	"""

	def testForSenseHatTemperatureUpdate(self):
		self.tempSensorAdaptorTask.updateSensorData()
		sensorDataCurrentValue = self.tempSensorAdaptorTask.sensorData.getCurrentValue()
		self.assertIsNotNone(sensorDataCurrentValue, 'Temperature Value is successfully supplied by SenseHat')

	'''
		Tests if there is an actuation, the color of the SenseHat is changed. Also tests for successful setters
		for Actuator class
	'''

	def testActuationTrigger(self):
		self.actuatorData = ActuatorData()
		self.actuatorData.setCommand('NORMAL')
		self.actuatorData.setName('TEMPERATURE')
		self.actuatorData.setValue(40.0)
		successfulActuation = self.tempActuatorAdaptor.updateActuator(self.actuatorData)
		self.assertTrue(successfulActuation, 'The color was successfully changed on the SenseHat')
		
	'''
		Tests if nominal temperature is successfully read from the configuration file
	'''	

	def testCheckNominalTemperatureUpdateValue(self):	
		nominalTemp = self.sensorDataManager.getNominalTempValue()
		self.assertIsNotNone(nominalTemp)
		
	'''
		Supplied the temperature value below the alerting threshold and checks if Green color is displayed on SenseHat
	'''

	def testCheckForNormalTempActuation(self):
		sensorData = SensorData()
		sensorData.addValue(19)
		actuation = self.sensorDataManager.handleSensorData(sensorData)
		self.assertTrue(actuation, 'Actuation Happened')
		actuationCommand = self.sensorDataManager.actuatorData.getCommand()
		self.assertEqual('NORMAL', actuationCommand, 'The commands for the actuator are same')
		
	'''
		Supplied the temperature value ABOVE the alerting threshold and checks if Red color is displayed on SenseHat
	'''

	def testCheckForHotTempActuation(self):
		sensorData = SensorData()
		sensorData.addValue(40)
		actuation = self.sensorDataManager.handleSensorData(sensorData)
		self.assertTrue(actuation, 'Actuation Happened')
		actuationCommand = self.sensorDataManager.actuatorData.getCommand()
		self.assertEqual('TOO HOT', actuationCommand, 'The commands for the actuator are same')
		
	'''
		Supplied the temperature value ABOVE the alerting threshold and checks if Blue color is displayed on SenseHat
	'''

	def testCheckForColdTempActuation(self):
		sensorData = SensorData()
		sensorData.addValue(-2)
		actuation = self.sensorDataManager.handleSensorData(sensorData)
		self.assertTrue(actuation, 'Actuation Happened')
		actuationCommand = self.sensorDataManager.actuatorData.getCommand()
		self.assertEqual('TOO COLD', actuationCommand, 'The commands for the actuator are same')
	
	
if __name__ == "__main__":
	unittest.main()
