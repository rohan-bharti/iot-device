from labs.module04.SensorDataManager import SensorDataManager
from labs.module04.HI2CSensorAdaptorTask import HI2CSensorAdaptorTask
from labs.module04.HumiditySensorAdaptorTask import HumiditySensorAdaptorTask
from labs.common.SensorData import SensorData

import unittest
import logging

"""
	Tests all the classes in the Module 3 package
"""


class Module04Test(unittest.TestCase):

	logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', level=logging.DEBUG, datefmt='%m/%d/%Y %I:%M:%S %p')

	"""
		Sets up the Initial Configuration for testing
	"""

	def setUp(self):
		self.sensorDataManager = SensorDataManager()
		self.hi2CSensorAdaptorTask = HI2CSensorAdaptorTask(self.sensorDataManager)
		self.humiditySensorAdaptorTask = HumiditySensorAdaptorTask(self.sensorDataManager)

	"""
		Tears down the dummy objects set before
	"""

	def tearDown(self):
		del self.hi2CSensorAdaptorTask
		del self.humiditySensorAdaptorTask
		del self.sensorDataManager
		
	"""
		Test successful humidity value retrieval from SMBus library.
	"""

	def testValidHumidityI2CValue(self):
		humidity = self.hi2CSensorAdaptorTask.getHumidity()
		self.assertGreater(humidity, 0.0, 'Humidity value is greater than 0')
		self.assertLessEqual(humidity, 100, 'Humidity value is less than 100')
		logging.info('TEST 1 PASSED: Valid Humidity Value is returned in case of SMBus.')
		
	"""
		Test successful humidity value retrieval from SenseHat library.
	"""

	def testValidHumiditySenseHatValue(self):
		humidity = self.humiditySensorAdaptorTask.getHumidity()
		self.assertGreater(humidity, 0.0, 'Humidity value is greater than 0')
		self.assertLessEqual(humidity, 100, 'Humidity value is less than 100')
		logging.info('TEST 2 PASSED: Valid Humidity Value is returned in case of SMBus.')
		
	'''
		Supplied the Humidity value (Humidity_SENSEHAT) checks if it is displayed on SenseHat. Also tests
		the underlying MultiActuatorAdaptor.
	'''

	def testCheckSenseHatHumidityActuation(self):
		sensorData = SensorData()
		sensorData.setName('Humidity_SENSEHAT')
		self.sensorDataManager.humiditySenseHatActuatorData.setName('Humidity_SENSEHAT')
		humidity = self.humiditySensorAdaptorTask.getHumidity()
		sensorData.addValue(humidity)
		actuation = self.sensorDataManager.handleSensorData(sensorData)
		self.assertTrue(actuation, 'Actuation Happened')
		actuationCommand = self.sensorDataManager.humiditySenseHatActuatorData.getCommand()
		self.assertEqual('SENSEHAT NOTIF', actuationCommand, 'The commands for the actuator are same')
		logging.info('TEST 3 PASSED: Tested for SENSEHAT Actuation Humidity Notification')	
		
	'''
		Supplied the Humidity value (Humidity_I2C) checks if it is displayed on SenseHat. Also tests
		the underlying MultiActuatorAdaptor.
	'''

	def testCheckI2CHumidityActuation(self):
		sensorData = SensorData()
		sensorData.setName('Humidity_I2C')
		self.sensorDataManager.humidityI2CActuatorData.setName('Humidity_I2C')
		humidity = self.hi2CSensorAdaptorTask.getHumidity()
		sensorData.addValue(humidity)
		actuation = self.sensorDataManager.handleSensorData(sensorData)
		self.assertTrue(actuation, 'Actuation Happened')
		actuationCommand = self.sensorDataManager.humidityI2CActuatorData.getCommand()
		self.assertEqual('I2CBUS NOTIF', actuationCommand, 'The commands for the actuator are same')
		logging.info('TEST 4 PASSED: Tested for I2C Actuation Humidity Notification')	
	
	'''
		Supplied the temperature value below the alerting threshold and checks if Green color is displayed on SenseHat.
		Also tests the underlying MultiActuatorAdaptor.
	'''

	def testCheckForNormalTempActuation(self):
		sensorData = SensorData()
		sensorData.setName('Temperature')
		self.sensorDataManager.tempActuatorData.setName('Temperature')
		sensorData.addValue(19)
		actuation = self.sensorDataManager.handleSensorData(sensorData)
		self.assertTrue(actuation, 'Actuation Happened')
		actuationCommand = self.sensorDataManager.tempActuatorData.getCommand()
		self.assertEqual('NORMAL', actuationCommand, 'The commands for the actuator are same')
		logging.info('TEST 5 PASSED: Tested for Normal Actuation Temperature Case')
		
	'''
		Supplied the temperature value ABOVE the alerting threshold and checks if Red color is displayed on SenseHat.
		Also tests the underlying MultiActuatorAdaptor.
	'''

	def testCheckForHotTempActuation(self):
		sensorData = SensorData()
		sensorData.setName('Temperature')
		self.sensorDataManager.tempActuatorData.setName('Temperature')
		sensorData.addValue(40)
		actuation = self.sensorDataManager.handleSensorData(sensorData)
		self.assertTrue(actuation, 'Actuation Happened')
		actuationCommand = self.sensorDataManager.tempActuatorData.getCommand()
		self.assertEqual('TOO HOT', actuationCommand, 'The commands for the actuator are same')
		logging.info('TEST 6 PASSED: Tested for HOT Actuation Temperature Case')
		
	'''
		Supplied the temperature value ABOVE the alerting threshold and checks if Blue color is displayed on SenseHat.
		Also tests the underlying MultiActuatorAdaptor.
	'''

	def testCheckForColdTempActuation(self):
		sensorData = SensorData()
		sensorData.setName('Temperature')
		self.sensorDataManager.tempActuatorData.setName('Temperature')
		sensorData.addValue(-2)
		actuation = self.sensorDataManager.handleSensorData(sensorData)
		self.assertTrue(actuation, 'Actuation Happened')
		actuationCommand = self.sensorDataManager.tempActuatorData.getCommand()
		self.assertEqual('TOO COLD', actuationCommand, 'The commands for the actuator are same')
		logging.info('TEST 7 PASSED: Tested for COLD Actuation Temperature Case')


if __name__ == "__main__":
	unittest.main()
