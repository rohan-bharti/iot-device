import unittest

from labs.module02.TempSensorEmulatorTask import TempSensorEmulatorTask
from labs.module02.SmtpClientConnector import SmtpClientConnector

"""
	Tests all the classes in the Module 2 package
"""
class Module02Test(unittest.TestCase):

	"""
		Sets up the dummy objects for testing
	"""
	def setUp(self):
		self.tempEmulator = TempSensorEmulatorTask()

	"""
		Tears down the dummy objects set before
	"""
	def tearDown(self):
		del self.tempEmulator
	
	"""
		Generates the Random Temp Values and checks if the Sensor data has been successfully updated or not
	"""
	def testRandomTempGenerationCount(self):
		self.tempEmulator.generateRandomTemperatureValue()
		sensorDataGenCount = self.tempEmulator.sensorData.getCount()
		self.assertEqual(1, sensorDataGenCount)
	
	"""
		Generates a Random Temp Value and checks if the Sensor data's value is between 0 and 30 or not
	"""	
	def testRandomTempGeneratedValue(self):
		self.tempEmulator.generateRandomTemperatureValue()
		r = range(31)
		self.assertTrue(self.tempEmulator.sensorData.curValue in r)
		
	"""
		Generates a Random Temp Value and checks if the Sensor data is successfully sent as an email 
	"""	
	def testSmtpClientConnectorEmail(self):
		self.tempEmulator.generateRandomTemperatureValue()
		self.tempEmulator.generateRandomTemperatureValue()
		dummySensorData = self.tempEmulator.getSensorData()
		smtpClient = SmtpClientConnector()
		try:
			smtpClient.serverSetupAndSendMessage(dummySensorData)
		except:
			self.fail('The server was not set up properly')

if __name__ == "__main__":
	unittest.main()