import unittest

from labs.module07.TempSensorAdaptorTask import TempSensorAdaptorTask
from labs.module07.CoapClientConnector import CoapClientConnector

"""
Test class for all requisite Module07 functionality. Needs to have the Server Running.
"""


class Module07Test(unittest.TestCase):

	"""
		Sets up the Initial Configuration for testing
	"""

	def setUp(self):
		self.tempSensorAdaptorTask = TempSensorAdaptorTask()
		self.coapClientConnector = CoapClientConnector()

	"""
		Tears down the dummy objects set before
	"""

	def tearDown(self):
		del self.coapClientConnector
		del self.tempSensorAdaptorTask

	"""
		Test the creation of SensorData
	"""

	def testSensorDataCreation(self):
		self.tempSensorAdaptorTask.updateSensorData()
		sensorData = self.tempSensorAdaptorTask.getSensorData()
		self.assertTrue(sensorData.getName() == 'Temperature')
		
	'''
		Test POST COAP Request
	'''

	def testCoapPostRequest(self):
		self.tempSensorAdaptorTask.updateSensorData()
		sensorData = self.tempSensorAdaptorTask.getSensorData()
		self.assertTrue(self.coapClientConnector.sendSensorData(sensorData))
		
	'''
		Test GET COAP Request
	'''

	def testCoapGetRequest(self):
		self.assertTrue(self.coapClientConnector.getSensorData())
	
	'''
		Test PUT COAP Request
	'''	

	def testCoapUpdateRequest(self):
		self.tempSensorAdaptorTask.updateSensorData()
		sensorData = self.tempSensorAdaptorTask.getSensorData()
		self.coapClientConnector.sendSensorData(sensorData)
		self.assertTrue(self.coapClientConnector.updateSensorData())
	
	'''
		Test DELETE COAP Request
	'''	

	def testCoapDeleteRequest(self):
		self.assertTrue(self.coapClientConnector.deleteSensorData())
		

if __name__ == "__main__":
	# import sys;sys.argv = ['', 'Test.testName']
	unittest.main()
