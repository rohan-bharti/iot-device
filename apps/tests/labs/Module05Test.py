from labs.module05.DeviceDataManager import DeviceDataManager
from labs.module05.HI2CSensorAdaptorTask import HI2CSensorAdaptorTask
from labs.module05.HumiditySensorAdaptorTask import HumiditySensorAdaptorTask

import unittest
import logging
from labs.module05.MultiSensorAdaptor import MultiSensorAdaptor

"""
Test class for all requisite Module05 functionality.
"""


class Module05Test(unittest.TestCase):

	logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', level=logging.DEBUG, datefmt='%m/%d/%Y %I:%M:%S %p')

	"""
		Sets up the Initial Configuration for testing
	"""

	def setUp(self):
		self.deviceDataManager = DeviceDataManager()
		self.multiSensorAdaptor = MultiSensorAdaptor()
		self.hi2CSensorAdaptorTask = HI2CSensorAdaptorTask(self.multiSensorAdaptor)
		self.humiditySensorAdaptorTask = HumiditySensorAdaptorTask(self.multiSensorAdaptor)

	"""
		Tears down the dummy objects set before
	"""

	def tearDown(self):
		del self.hi2CSensorAdaptorTask
		del self.humiditySensorAdaptorTask
		del self.deviceDataManager
		
	"""
		Test successful humidity value retrieval from SMBus library.
	"""

	def testValidHumidityI2CValue(self):
		humidity = self.hi2CSensorAdaptorTask.getHumidity()
		self.assertGreater(humidity, 0.0, 'Humidity value is greater than 0')
		self.assertLessEqual(humidity, 100, 'Humidity value is less than 100')
		logging.info('TEST 1 PASSED: Valid Humidity Value is returned in case of SMBus.')
		
	"""
		Test successful humidity value retrieval from SenseHat library.
	"""

	def testValidHumiditySenseHatValue(self):
		humidity = self.humiditySensorAdaptorTask.getHumidity()
		self.assertGreater(humidity, 0.0, 'Humidity value is greater than 0')
		self.assertLessEqual(humidity, 100, 'Humidity value is less than 100')
		logging.info('TEST 2 PASSED: Valid Humidity Value is returned in case of SMBus.')

if __name__ == "__main__":
	# import sys;sys.argv = ['', 'Test.testName']
	unittest.main()
