from labs.module08.MqttClientConnector import MqttClientConnector
from labs.common.SensorData import SensorData
from labs.common.ActuatorData import ActuatorData
from paho.mqtt.client import MQTTMessage

import unittest
from labs.module08.MultiActuatorAdaptor import MultiActuatorAdaptor


"""
Test class for all requisite Module08 functionality. Needs to have the Server Running.
"""
class Module08Test(unittest.TestCase):

	"""
		Sets up the Initial Configuration for testing
	"""
	def setUp(self):
		self.mqttClient = MqttClientConnector()
		self.mqttClient.setupMqttClient()
		self.multiActuatorAdaptor = MultiActuatorAdaptor()

	"""
		Tears down the dummy objects set before
	"""

	def tearDown(self):
		del self.mqttClient
		del self.multiActuatorAdaptor

	"""
		Tests publishing the SensorData.
	"""

	def testPublishingSensorData(self):
		sensorData = SensorData()
		sensorData.setName("Temperature")
		sensorData.addValue(20)
		publishStatus = self.mqttClient.publishSensorData(sensorData, 0)
		self.assertTrue(publishStatus, "The Publish Process wasn't successful!")
	
	"""
		Tests publishing the ActuatorData.
	"""	

	def testPublishingActuatorData(self):
		actuatorData = ActuatorData()
		actuatorData.setCommand("TOO HOT")
		actuatorData.setName("TEMP")
		actuatorData.setValue(20)
		publishStatus = self.mqttClient.publishActuatorCommand(actuatorData, 0)
		self.assertTrue(publishStatus, "The publish process was not successful!")

if __name__ == "__main__":
	#import sys;sys.argv = ['', 'Test.testName']
	unittest.main()