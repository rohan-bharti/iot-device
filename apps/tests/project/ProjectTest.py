import unittest
from project.protocol.MqttClientConnector import MqttClientConnector
from project.util.SensorData import SensorData
from project.util.ActuatorData import ActuatorData

from labs.module07.TempSensorAdaptorTask import TempSensorAdaptorTask

from project.adaptor.MultiActuatorAdaptor import MultiActuatorAdaptor
from project.protocol.CoapClientConnector import CoapClientConnector

"""
	Test class for all requisite Project Device App functionalities.
"""
class ProjectTest(unittest.TestCase):

	"""
		Sets up the Initial Configuration for testing
	"""
	def setUp(self):
		self.mqttClient = MqttClientConnector()
		self.mqttClient.setupMqttClient()
		self.multiActuatorAdaptor = MultiActuatorAdaptor()
		self.coapClientConnector = CoapClientConnector()
		self.tempSensorAdaptorTask = TempSensorAdaptorTask()

	"""
		Tears down the dummy objects set before
	"""

	def tearDown(self):
		del self.mqttClient
		del self.multiActuatorAdaptor

	"""
		Tests publishing the SensorData.
	"""

	def testPublishingSensorData(self):
		sensorData = SensorData()
		sensorData.setName("Temperature")
		sensorData.addValue(20)
		publishStatus = self.mqttClient.publishSensorData(sensorData, 0)
		self.assertTrue(publishStatus, "The Publish Process wasn't successful!")
	
	"""
		Tests publishing the ActuatorData.
	"""	

	def testPublishingActuatorData(self):
		actuatorData = ActuatorData()
		actuatorData.setCommand("TOO HOT")
		actuatorData.setName("TEMP")
		actuatorData.setValue(20)
		publishStatus = self.mqttClient.publishActuatorCommand(actuatorData, 0)
		self.assertTrue(publishStatus, "The publish process was not successful!")
		
	"""
		Test the creation of SensorData
	"""

	def testSensorDataCreation(self):
		self.tempSensorAdaptorTask.updateSensorData()
		sensorData = self.tempSensorAdaptorTask.getSensorData()
		self.assertTrue(sensorData.getName() == 'Temperature')
		
	'''
		Test POST COAP Request
	'''

	def testCoapPostRequest(self):
		self.tempSensorAdaptorTask.updateSensorData()
		sensorData = self.tempSensorAdaptorTask.getSensorData()
		self.assertTrue(self.coapClientConnector.postTempSensorData(sensorData))
		
	'''
		Test GET COAP Request
	'''

	def testCoapGetRequest(self):
		self.assertTrue(self.coapClientConnector.getSensorData("tempSensorData"))
	
	'''
		Test DELETE COAP Request
	'''	

	def testCoapDeleteRequest(self):
		self.assertTrue(self.coapClientConnector.deleteSensorData("tempSensorData"))
		

if __name__ == "__main__":
	#import sys;sys.argv = ['', 'Test.testName']
	unittest.main()