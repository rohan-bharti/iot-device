import unittest

from labs.common.SensorData import SensorData
from labs.common.DataUtil import DataUtil
from labs.common.ActuatorData import ActuatorData

"""
Test class for all requisite DataUtil functionality.
"""


class DataUtilTest(unittest.TestCase):

	"""
		Sets up the objects required for testing
	"""

	def setUp(self):
		self.dataUtil = DataUtil()
		
		self.sensorData = SensorData()
		self.sensorData.addValue(20)
		self.sensorData.setName('Temperature')
		
		self.actuatorData = ActuatorData()
		self.actuatorData.createCustomActuatorData('HOT TEMP', 20, 'TEMP')

	"""
		Tears down the objects in setUp() method.
	"""

	def tearDown(self):
		del self.dataUtil
		del self.sensorData
	
	"""
		Test to check SensorData to Json String Conversion
	"""

	def testToJsonFromSensorData(self):
		str = self.dataUtil.toJsonFromSensorData(self.sensorData)
		self.assertIsNotNone(str, 'The Json String was not generated from the SensorData Object')
		
	"""
		Test to check SensorData to Json String Conversion
	"""

	def testToSensorDataFromJson(self):
		jsonString = "{\"timeStamp\": \"2020-02-20 00:26:41.758011\", \"sampleCount\": 1, \"curValue\": 20, \"totValue\": 20, \"minValue\": 20, \"maxValue\": 20, \"avgValue\": 20.0, \"name\": \"Temperature\"}"
		sensorDataObject = self.dataUtil.toSensorDataFromJson(jsonString)
		self.assertIsNotNone(sensorDataObject, 'The Json String was not generated from the SensorData Object')
		self.assertEqual('Temperature', sensorDataObject.getName(), 'The Name attribute does not match')
		self.assertEqual(20.0, sensorDataObject.getCurrentValue(), 'The Current Values does not match')
		
	"""
		Test to read SensorData from the File
	"""

	def testReadAndWriteSensorDataFromFile(self):
		check = self.dataUtil.writeSensorDataToFile(self.sensorData)
		self.assertTrue(check, 'The data was not written to the file')
		sensorDataObject = self.dataUtil.toSensorDataFromJsonFile('sensorData.txt')
		self.assertIsNotNone(sensorDataObject, 'The Json String was not generated from the SensorData Object')
		self.assertEqual('Temperature', sensorDataObject.getName(), 'The Name attribute does not match')
		self.assertEqual(20.0, sensorDataObject.getCurrentValue(), 'The Current Values does not match')
	
	"""
		Test to check ActuatorData to Json String Conversion
	"""

	def testToJsonFromActuatorData(self):
		str = self.dataUtil.toJsonFromSensorData(self.actuatorData)
		self.assertIsNotNone(str, 'The Json String was not generated from the ActuatorData Object')	

	"""
		Test to check ActuatorData to Json String Conversion
	"""

	def testToActuatorDataFromJson(self):
		jsonString = "{\"command\": \"HOT TEMP\", \"value\": 20, \"name\": \"TEMP\"}"
		actuatorDataObject = self.dataUtil.toActuatorDataFromJson(jsonString)
		self.assertIsNotNone(actuatorDataObject, 'The Json String was not generated from the ActuatorData Object')
		self.assertEqual('HOT TEMP', actuatorDataObject.getCommand(), 'The Command attribute does not match')
		self.assertEqual(20, actuatorDataObject.getValue(), 'The Actuator Values does not match')
		
	"""
		Test to read ActuatorData from the File
	"""

	def testWriteAndReadActuatorDataFromFile(self):
		check = self.dataUtil.writeActuatorDataToFile(self.actuatorData)
		self.assertTrue(check, 'The data was not written to the file')
		actuatorDataObject = self.dataUtil.toActuatorDataFromJsonFile('actuatorData.txt')
		self.assertIsNotNone(actuatorDataObject, 'The Json String was not generated from the ActuatorData Object')
		self.assertEqual('HOT TEMP', actuatorDataObject.getCommand(), 'The Command attribute does not match')
		self.assertEqual(20, actuatorDataObject.getValue(), 'The Actuator Values does not match')
	

if __name__ == "__main__":
	# import sys;sys.argv = ['', 'Test.testName']
	unittest.main()
