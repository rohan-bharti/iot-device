from labs.common.ActuatorData import ActuatorData

import unittest

"""
	Test class for the ActuatorData module.
"""


class ActuatorDataTest(unittest.TestCase):

	"""
		Sets up the ActuatorData with a few dummy values
	"""

	def setUp(self):
		self.actuatorData = ActuatorData()
		self.actuatorData.setCommand('TESTCOMMAND')
		self.actuatorData.setName('TESTNAME')
		self.actuatorData.setValue(24.0)

	"""
		Tear downs the Actuator Data instance defined
	"""

	def tearDown(self):
		del self.actuatorData
	
	"""
		Tests fetching the Command value
	"""

	def testGetAverageValue(self):
		self.assertEqual('TESTCOMMAND', self.actuatorData.getCommand())
	
	"""
		Tests fetching the Name of the Actuation
	"""	

	def testGetCountValue(self):
		self.assertEqual('TESTNAME', self.actuatorData.getName())
	
	"""
		Tests fetching the Value of the Actuation
	"""		

	def testGetCurrentValue(self):
		self.assertEqual(24.0, self.actuatorData.getValue())
	

if __name__ == "__main__":
	unittest.main()
