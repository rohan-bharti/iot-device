import unittest
from labs.common.ConfigUtil import ConfigUtil
import logging

"""
Test class for the ConfigUtil module.
"""

class ConfigUtilTest(unittest.TestCase):


	logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', level=logging.DEBUG, datefmt='%m/%d/%Y %I:%M:%S %p')

	"""
	Use a Test Config File to set up the ConfigUtil class.
	"""
	def setUp(self):
		logging.info('SETTING UP THE CONFIG UTIL CLASS FOR TESTING')
		self.configUtil = ConfigUtil('../../../config/TestConfig.props')

	"""
	Use this to tear down any allocated resources after your tests are complete. This
	is where you may want to release connections, zero out any long-term data, etc.
	"""
	def tearDown(self):
		logging.info('DELETING THE CONFIG UTIL AFTER TESTING')
		del self.configUtil
	
	"""
	Tests retrieval of a boolean property.
	"""
	def testGetBooleanProperty(self):
		booleanValue = self.configUtil.getBooleanValue('FILLED SECTION', 'handsome')
		self.assertTrue(booleanValue)
	
	"""
	Tests retrieval of an integer property.
	"""
	def testGetIntegerProperty(self):
		integerValue = self.configUtil.getIntegerValue('FILLED SECTION', 'age')
		if(isinstance(integerValue, int)):
			pass
		else:
			self.fail("Integer wasn't fetched")
	
	"""
	Tests retrieval of a string property.
	"""
	def testGetProperty(self):
		stringProperty = self.configUtil.getValue('FILLED SECTION', 'firstname')
		if stringProperty == 'Rohan':
			pass
		else:
			self.fail("String Not Found")
	
	"""
	Tests if a property exists.
	"""
	def testHasProperty(self):
		keyPresent = self.configUtil.checkKey('FILLED SECTION', 'handsome')
		if keyPresent:
			pass
		else:
			self.fail("Property doesn't exist")

	"""
	Tests if a section exists.
	"""
	def testHasSection(self):
		sectionPresent = self.configUtil.hasSection()
		if sectionPresent:
			pass
		else:
			self.fail("Section not found")
	
	"""
	Tests if the configuration is loaded.
	"""
	def testIsConfigDataLoaded(self):
		if self.configUtil.hasConfigData():
			pass
		else:
			self.fail("Configuration data not loaded.")
	
if __name__ == "__main__":
	unittest.main()
