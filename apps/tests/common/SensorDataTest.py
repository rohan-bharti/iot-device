import unittest
import logging

from labs.common.SensorData import SensorData

"""
Test class for all requisite SensorData functionality.
"""
class SensorDataTest(unittest.TestCase):

	logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', level=logging.DEBUG, datefmt='%m/%d/%Y %I:%M:%S %p')

	"""
		Sets up the SensorDataObject with a few dummy values
	"""
	def setUp(self):
		logging.info('SETTING UP THE SENSOR DATA CLASS FOR TESTING')
		self.sensorData = SensorData()
		self.sensorData.addValue(12)
		self.sensorData.addValue(24)
		self.sensorData.addValue(36)
		self.sensorData.setName("Temperature")

	"""
		Tear downs the Sensor Data instance defined
	"""
	def tearDown(self):
		logging.info('DELETING THE SENSOR DATA OBJECT AFTER TESTING')
		del self.sensorData
	
	"""
		Tests fetching the average value
	"""
	def testGetAverageValue(self):
		self.assertEqual(24.0,self.sensorData.getAverageValue())
	
	"""
		Tests fetching the total number of times the sensor data has been updated
	"""	
	def testGetCountValue(self):
		self.assertEqual(3, self.sensorData.getCount())
	
	"""
		Tests fetching the latest value supplied to the sensor data
	"""		
	def testGetCurrentValue(self):
		self.assertEqual(36.0, self.sensorData.getCurrentValue())
	
	"""
		Tests fetching the max value of temperature supplied
	"""		
	def testGetMaxValue(self):
		self.assertEqual(36.0, self.sensorData.getMaxValue())
	
	"""
		Tests fetching the minimum temperature value supplied
	"""		
	def testGetMinValue(self):
		self.assertEqual(12.0, self.sensorData.getMinValue())
	
	"""
		Tests fetching the name of the Sensor Data Metric supplied
	"""		
	def testGetNameValue(self):
		self.assertEqual("Temperature", self.sensorData.getName())

if __name__ == "__main__":
	unittest.main()