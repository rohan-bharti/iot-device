from project.DeviceDataManager import DeviceDataManager

'''
    Script to start the application
'''

deviceDataManager = DeviceDataManager()

# Starts fetching values from the SenseHat and pushing them to the CoAP Server
deviceDataManager.kickOffUpdatingHumiditySensorData()
deviceDataManager.kickOffUpdatingPressureSensorData()
deviceDataManager.kickOffUpdatingTempertureSensorData()

#Start the Mqtt Client Connector and listen for the incoming Ubidots Actuation
deviceDataManager.subscribeToActuatorDataFromGateway()