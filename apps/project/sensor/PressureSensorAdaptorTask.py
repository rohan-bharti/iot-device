from threading import Thread
from sense_hat import SenseHat
from random import randint

import logging
import time

from project.util.SensorData import SensorData

'''
    Makes use of the Sense Hat Library to get the Pressure
'''


class PressureSensorAdaptorTask(Thread):
    
    logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', level=logging.DEBUG, datefmt='%m/%d/%Y %I:%M:%S %p')
    
    '''
        Constructor - Initializes SensorData and SenseHat
    '''

    def __init__(self, deviceDataManager):
        Thread.__init__(self)
        self.sensorData = SensorData()
        self.sensorData.setName('Pressure')
        self.senseHat = SenseHat()
        self.deviceDataManager = deviceDataManager
        
    '''
        Fetches the Pressure from the SenseHat
    '''

    def getPressure(self):
        return self.senseHat.get_pressure()
        
    '''
         Updates the SensorData with the received SenseHat Pressure Value
    '''

    def updateSensorData(self):
        pressure = self.getPressure()
        self.sensorData.addValue(pressure) 
        
    '''
        Runs continuously and updates the SensorData with the latest Pressure reading supplied by the SenseHat
    '''

    def run(self):
        while True:
            self.updateSensorData()
            logging.info('Updated the SensorData Object, Pressure from SENSEHAT: ' + str(self.sensorData.getCurrentValue()))
            self.deviceDataManager.handleSensorData(self.sensorData)
            secondsToSleep = randint(60, 70)
            time.sleep(secondsToSleep)
