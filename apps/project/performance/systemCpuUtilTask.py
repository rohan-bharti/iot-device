import psutil
import logging

'''
    Class to monitor the System CPU Utilization
'''

class SystemCpuUtilTask:
    
    #constructor for the class
    def __init__(self):
        self._cpuUtil = 0.0
    
    #setter for cpuUtil
    def set_cpuUtil(self, cpuUtil):
        self._cpuUtil = cpuUtil   
            
    #getter for cpuUtil       
    def get_cpuMem(self):
        return self._cpuUtil 
    
    #uses the psutil library to fetch the CPU Utilization Percentage 
    def getDataFromSensor(self):
        try:
            self.set_cpuUtil(psutil.cpu_percent(interval=1))  
        except Exception as e:
            logging.error(e)
        return self.get_cpuMem()
    