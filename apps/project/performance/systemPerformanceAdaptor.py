from random import randint

import logging
import time
from threading import Thread
from datetime import datetime

from project.performance.systemCpuUtilTask import SystemCpuUtilTask
from project.performance.systemMemUtilTask import SystemMemUtilTask

'''
    Class to display CPU and Memory Utilization
'''

class SystemPerformanceAdaptor(Thread):
    
    def __init__(self):
        Thread.__init__(self)
        self.logger = logging.getLogger('systemPerformance')
        self.logger.setLevel(logging.DEBUG)
        # create file handler which logs even debug messages
        fh = logging.FileHandler('performance.log')
        fh.setLevel(logging.DEBUG)
        self.logger.addHandler(fh)
        self.systemCpuUtilTask = SystemCpuUtilTask()
        self.systemMemUtilTask = SystemMemUtilTask()
        
    #logger to display well formatted information
    def run(self):
        while True:
            cpuUtil = self.systemCpuUtilTask.getDataFromSensor()
            cpuMem = self.systemMemUtilTask.getDataFromSensor()
            self.logger.info(str(datetime.now()) + '- CPU UTIL: ' + str(cpuUtil))
            self.logger.info(str(datetime.now()) + '- MEM USAGE: ' + str(cpuMem))
            secondsToSleep = randint(20, 60)
            time.sleep(secondsToSleep)
        
    