import psutil
import logging

'''
    Class to monitor the System CPU Virtual Memory Usage
'''

class SystemMemUtilTask:
    
    #constructor for the class
    def __init__(self):
        self._cpuMem = 0.0
        
    #setter for cpuMem
    def set_cpuMem(self, cpuMem):
        self._cpuMem = cpuMem   
    
    #getter for cpuMem      
    def get_cpuMem(self):
        return self._cpuMem        
    
    #uses the psutil library to fetch the CPU Virtual Memory Usage Percentage
    def getDataFromSensor(self):
        try:
            self.set_cpuMem(psutil.virtual_memory().percent) 
        except Exception as e:
            logging.error(e)
        return self.get_cpuMem()