from datetime import datetime

'''
    Model class to store the Sensor Data
'''
class SensorData():
    timeStamp = None
    name = 'Not set'
    curValue = 0
    avgValue = 0
    minValue = 0
    maxValue = 0
    totValue = 0
    sampleCount = 0
    
    def __init__(self):
        self.timeStamp = str(datetime.now())

    def createCustomSensorData(self, timeStamp, name, curValue, avgValue, minValue, maxValue, totValue, sampleCount):
        self.timeStamp = timeStamp
        self.name = name
        self.curValue = curValue
        self.avgValue = avgValue
        self.minValue = minValue
        self.maxValue = maxValue
        self.totValue = totValue
        self.sampleCount = sampleCount
   
    '''
        Add new value to the Sensor Data
    '''
    def addValue(self, newVal):
        self.sampleCount += 1
        self.timeStamp = str(datetime.now())
        self.curValue = newVal
        self.totValue += newVal
        
        #Setting the min to whatever is supplied during the first iteration
        if (self.sampleCount==1):
            self.minValue = self.curValue
        elif (self.curValue < self.minValue):
            self.minValue = self.curValue
            
        if (self.curValue > self.maxValue):
            self.maxValue = self.curValue
        if (self.totValue != 0 and self.sampleCount > 0):
            self.avgValue = self.totValue / self.sampleCount
       
    '''
         Gets the time stamp value
    '''
    def getTimeStamp(self):
        return self.timeStamp
    
    '''
        Get the average value of the sensor data supplied so far
    '''        
    def getAverageValue(self):
        return float(self.avgValue)
    
    '''
        Gets the count of the number of times the addValue() function is called
    '''  
    def getCount(self):
        return float(self.sampleCount)
    
    '''
        Gets the most recently value added to the sensor Data
    ''' 
    def getCurrentValue(self):
        return float(self.curValue)
    
    '''
        Gets the Max Value of the Sensor Data supplied
    '''
    def getMaxValue(self):
        return float(self.maxValue)     
    
    '''
        Gets the Min Value of the Sensor Data supplied
    '''
    def getMinValue(self):
        return float(self.minValue)
    
        
    '''
        Gets the name of the type of Sensor Data supplied
    '''
    def getName(self):
        return self.name
    
    '''
        Sets the name of the type of Sensor Data supplied
    '''
    def setName(self, name):
        self.name = name
        
    '''
        Gets the sample count of Sensor Data
    '''
    def getSampleCount(self):
        return self.sampleCount