import configparser
import logging
from project.util.ConfigConst import ConfigConst

''' Config Class to read the Configuration file and fetch data from it '''

class ConfigUtil:
    
    logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', level=logging.DEBUG, datefmt='%m/%d/%Y %I:%M:%S %p')
        
    def __init__(self, filePath="None"):
        self.config = configparser.ConfigParser()
        self.loadConfig(filePath)
        
    '''
        Fetches the value of the key in the section supplied.
    '''
    def getValue(self, section, key):
        try:
            return self.config[section][key]
        except:
            return 'NOT FOUND'
        
    '''
        Fetches the Integer value of the key in the section supplied.
    '''
    def getIntegerValue(self, section, key):
        try:
            return self.config.getint(section, key)
        except:
            return 'INTEGER NOT FOUND'
        
    '''
        Fetches the value of the key in the section supplied.
    '''
    def getBooleanValue(self, section, key):
        try:
            return self.config.getboolean(section, key)
        except:
            return 'BOOLEAN NOT FOUND'
        
    '''
        Checks if a Key, Value Pair exists in any of the sections, if they exist.
    '''
    def hasConfigData(self):
        if self.hasSection():
            emptyKeyValuePairSections = 0
            totalSections = len(self.config.sections())
            for section in self.config.sections():
                    if not self.sectionHasKeyValuePairs(section):
                        emptyKeyValuePairSections+=1
            if(emptyKeyValuePairSections==totalSections):
                logging.error('NO KEY, VALUE PAIR EXISTS IN ANY OF THE SECTIONS')
                return False  
        else:
            return False  
        return True
    
    '''
        Reads the Config File's Path supplied.
    '''
    def loadConfig(self, relativeFilePath):
        if relativeFilePath is None:
            try:
                self.config.read(ConfigConst.CONFIGURATION_FILE_PATH)
                logging.info("The file loaded successfully")
                return True
            except:
                logging.error("The file wasn't loaded successfully")
                return False
        else:
            try:
                self.config.read(relativeFilePath)
                logging.info("The file loaded successfully")
                return True
            except:
                logging.error("The file wasn't loaded successfully")
                return False
    
    '''
        Checks if a section exists in the Config File
    '''
    def hasSection(self):
        if not self.config.sections():
            logging.error('NO SECTIONS EXIST')
            return False
        return True
        
    '''
        Checks if there exists a key-value pair in a section
    '''
    def sectionHasKeyValuePairs(self, section):
        if section == 'root':
            return True
        if not self.config.items(section, False, None):
            return False
        return True
    
    '''
        Checks if the key given exists in the Configuration file or not
    '''
    def checkKey(self, section, key): 
        if self.config.has_option(section, key): 
            return True
        else: 
            return False
        
        