from project.util.ActuatorData import ActuatorData
import logging

logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', level=logging.DEBUG, datefmt='%m/%d/%Y %I:%M:%S %p')


class ActuatorDataListener:
    
    actuatorData = ActuatorData()
    
    '''
        Callback function to get the actuatorData from the channel subscribed
    '''

    def onMessage(self, actuatorData):
        self.actuatorData = actuatorData
       
    '''
        Helper method to retrieve the actuatorData supplied by Gateway App
    ''' 

    def getActautorData(self):
        return self.actuatorData 
