from project.protocol.CoapClientConnector import CoapClientConnector
from project.sensor.TempSensorAdaptorTask import TempSensorAdaptorTask
from project.sensor.PressureSensorAdaptorTask import PressureSensorAdaptorTask
from project.sensor.HumiditySensorAdaptorTask import HumiditySensorAdaptorTask

import logging

'''
    Returns an instance of the CoapClientConnector
'''


class MultiSensorAdaptor:
    
    logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', level=logging.DEBUG, datefmt='%m/%d/%Y %I:%M:%S %p')
    
    '''
        Sets up the CoapClient
    '''
    
    def setupCoapClient(self):
        coapClient = CoapClientConnector()
        return coapClient
    
    '''
        Starts the temperature polling
    '''

    def startReadingTemperature(self, deviceDataManager):
        tempSensorAdaptorTask = TempSensorAdaptorTask(deviceDataManager)
        logging.info('Started Polling temperature')
        tempSensorAdaptorTask.start()
        
    '''
        Starts the Humidity polling from SenseHat
    '''

    def startReadingHumidity(self, deviceDataManager):
        humiditySensorAdaptorTask = HumiditySensorAdaptorTask(deviceDataManager)
        logging.info('Started Polling humidity')
        humiditySensorAdaptorTask.start()   
        
    '''
        Starts the Pressure polling from SenseHat
    '''

    def startReadingPressure(self, deviceDataManager):
        pressureSensorAdaptorTask = PressureSensorAdaptorTask(deviceDataManager)
        logging.info('Started Polling humidity')
        pressureSensorAdaptorTask.start()
        
