import logging
from project.util.DataUtil import DataUtil
from project.util.ActuatorDataListener import ActuatorDataListener
from project.util.ConfigUtil import ConfigUtil

from coapthon.client.helperclient import HelperClient

logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', level=logging.DEBUG, datefmt='%m/%d/%Y %I:%M:%S %p')

tempPath = "tempSensorData"
humidityPath = "humiditySensorData"
pressurePath = "pressureSensorData"

'''
    CoapClientConnector class. Sets up the CoAPClient.
'''


class CoapClientConnector:
    
    '''
        Constructor
    '''

    def __init__(self):
        self.dataUtil = DataUtil()
        self.configUtil = ConfigUtil(None)
        self.client = None
        self.sentSensorData = None
        self.setupCoapClient()
        
    '''
        Register ActuatorDataListener
    '''
        
    def registerActuatorDataListener(self):
        self.actuatorDataListener = ActuatorDataListener()
        
    '''
        Register CoapClient
    '''

    def setupCoapClient(self):
        if self.configUtil.hasConfigData():
            host = self.configUtil.getValue('coap.device', 'host')
            port = self.configUtil.getIntegerValue('coap.device', 'port')
            self.client = HelperClient(server=(host, port))
        else:
            logging.error("The CoAP Client wasn't configured successfully")
        
    '''
        Sends Temperature SensorData to the CoAP Server, makes a POST Request
    '''
        
    def postTempSensorData(self, sensorData):
        if self.client != None:
            self.sentSensorData = sensorData
            sensorDataString = self.dataUtil.toJsonFromSensorData(sensorData)
            logging.info("CoAP POST TEMPERATURE: JSON BEFORE: " + sensorDataString)
            self.client.post(tempPath, sensorDataString)
            return True
        else:
            logging.error("Cannot Make a POST CoAP TEMP request, Client is not setup")
            return False
        
    '''
        Sends Humidity SensorData to the CoAP Server, makes a POST Request
    '''
        
    def postHumiditySensorData(self, sensorData):
        if self.client != None:
            self.sentSensorData = sensorData
            sensorDataString = self.dataUtil.toJsonFromSensorData(sensorData)
            logging.info("CoAP POST HUMIDITY: JSON BEFORE: " + sensorDataString)
            self.client.post(humidityPath, sensorDataString)
            return True
        else:
            logging.error("Cannot Make a POST CoAP HUMIDITY request, Client is not setup")
            return False
        
    '''
        Sends Pressure SensorData to the CoAP Server, makes a POST Request
    '''
        
    def postPressureSensorData(self, sensorData):
        if self.client != None:
            self.sentSensorData = sensorData
            sensorDataString = self.dataUtil.toJsonFromSensorData(sensorData)
            logging.info("CoAP POST PRESSURE: JSON BEFORE: " + sensorDataString)
            self.client.post(pressurePath, sensorDataString)
            return True
        else:
            logging.error("Cannot Make a POST CoAP PRESSURE request, Client is not setup")
            return False
    
    '''
        Gets SensorData to the CoAP Server, makes a GET Request
    '''
         
    def getSensorData(self, path):
        if self.client != None:
            response = self.client.get(path)
            logging.info(response.pretty_print())   
            return True
        else:
            logging.error("Cannot Make a GET request, Client is not setup")
            return False
        
    '''
        Sends SensorData to the CoAP Server, makes a PUT Request
    '''
        
    def updateSensorData(self, path):
        if self.client != None:
            sensorData = self.sentSensorData
            if sensorData == None:
                logging.info("Cannot retrieve the sent SensorData successfully!")
                return False
            sensorData.setName("Updated Temp SensorData")
            sensorDataString = self.dataUtil.toJsonFromSensorData(sensorData)
            logging.info("JSON BEFORE: " + sensorDataString)
            response = self.client.put(path, sensorDataString)
            logging.info(response.pretty_print())
            return True
        else:
            logging.error("Cannot Make a PUT request, Client is not setup")
            return False
    
    '''
        Deletes SensorData from the CoAP Server, makes a DELETE Request
    '''
    
    def deleteSensorData(self, path):
        if self.client != None:
            response = self.client.delete(path)
            logging.info(response.pretty_print())
            return True
        else:
            logging.error("Cannot Make a DELETE request, Client is not setup")
            return False
        
