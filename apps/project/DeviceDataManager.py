import logging

from project.adaptor.MultiSensorAdaptor import MultiSensorAdaptor
from project.protocol.MqttClientConnector import MqttClientConnector
from project.performance.systemPerformanceAdaptor import SystemPerformanceAdaptor

logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', level=logging.DEBUG, datefmt='%m/%d/%Y %I:%M:%S %p')

'''
    DataManager class, polls Humidity, Temperature and Pressure, s.
'''


class DeviceDataManager:
    
    sensorData = None
    
    '''
        Constructor
    '''
    
    def __init__(self):
        self.multiSensorAdaptor = MultiSensorAdaptor()
        self.coapClient = MultiSensorAdaptor.setupCoapClient(self)
        self.systemPerformanceAdaptor = SystemPerformanceAdaptor().start()
        
    '''
        Starts the temperature Polling process
    '''

    def kickOffUpdatingTempertureSensorData(self):
        self.multiSensorAdaptor.startReadingTemperature(self)
        
    '''
        Starts the Humidity Polling process
    '''

    def kickOffUpdatingHumiditySensorData(self):
        self.multiSensorAdaptor.startReadingHumidity(self)
        
    '''
        Starts the Pressure Polling process
    '''

    def kickOffUpdatingPressureSensorData(self):
        self.multiSensorAdaptor.startReadingPressure(self)
        
    '''
        Sets up the MQTT Client
    '''    

    def setupMqttClient(self):
        self.mqttClient = MqttClientConnector()

    '''
        Starts listening for the ActuatorData on the channel on MQTT Broker
    '''  

    def subscribeToActuatorDataFromGateway(self):
        self.setupMqttClient()
        self.mqttClient.subscribeToActuatorCommands(1)
        self.mqttClient.start()
        
    '''
        Handles the SensorData and makes a POST Coap Request to the Gateway App supplying the SensorData
    '''

    def handleSensorData(self, sensorData):
        
        # Check if the SensorData is for Temperature
        if sensorData.getName() == 'Temperature':
            self.coapClient.postTempSensorData(sensorData)
            
        # Check if the SensorData is for Humidity
        if sensorData.getName() == 'Humidity':
            self.coapClient.postHumiditySensorData(sensorData)
        
        # Check if the SensorData is for Pressure
        if sensorData.getName() == 'Pressure':
            self.coapClient.postPressureSensorData(sensorData)

