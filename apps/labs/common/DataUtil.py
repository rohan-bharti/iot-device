import logging
import json
from labs.common.SensorData import SensorData
from labs.common.ActuatorData import ActuatorData

logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', level=logging.DEBUG, datefmt='%m/%d/%Y %I:%M:%S %p')

'''
    DataUtil Class for managing Json and SensorData/ActuatorData conversion
'''


class DataUtil:
    
    '''
        Converts SensorData to Json String
    '''

    def toJsonFromSensorData(self, sensorData):
        strData = json.dumps(sensorData.__dict__) 
        return strData
    
    '''
        Converts Json String to SensorData
    '''    

    def toSensorDataFromJson(self, jsonDataString):
        obj = json.loads(jsonDataString)
        sensorData = SensorData()
        sensorData.createCustomSensorData(**obj)
        return sensorData
    
    '''
        Write SensorData to a File
    ''' 

    def writeSensorDataToFile(self, sensorData):
        sensorDataList = {}
        sensorDataList.update(vars(sensorData))
        try:
            with open("sensorData.txt", "w") as file:
                json.dump(sensorDataList, file)
            return True
        except:
            logging.error("SensorData not written to the file")
            return False
    
    '''
        Read JsonData from a file and convert to SensorData
    ''' 

    def toSensorDataFromJsonFile(self, file):
        try:
            with open(file) as json_data:
                data = json.load(json_data)
                sensorData = SensorData()
                sensorData.createCustomSensorData(**data)
                return sensorData    
        except:
            logging.error("Json Data Wasn't successfully read from the file")
            return;
    
    '''
        Converts ActuatorData to Json String
    '''

    def toJsonFromActuatorData(self, actuatorData):
        strData = json.dumps(actuatorData.__dict__) 
        return strData
    
    '''
        Converts Json to ActuatorData
    '''    

    def toActuatorDataFromJson(self, jsonString):
        logging.info(jsonString)
        jsonObj = json.loads(jsonString)
        actuatorData = ActuatorData()
        actuatorData.createCustomActuatorData(**jsonObj)
        return actuatorData
     
    '''
        Write ActuatorData to a File
    '''     

    def writeActuatorDataToFile(self, actuatorData):
        actuatorDataList = {}
        actuatorDataList.update(vars(actuatorData))
        try:
            with open("actuatorData.txt", "w") as file:
                json.dump(actuatorDataList, file)
            return True
        except:
            logging.error("ActuatorData not written to the file")
            return False
    
    '''
        Read JsonData from a file and convert to ActuatorData
    '''         

    def toActuatorDataFromJsonFile(self, jsonFile):
        try:
            with open(jsonFile) as json_data:
                data = json.load(json_data)
                actuatorData = ActuatorData()
                actuatorData.createCustomActuatorData(**data)
                return actuatorData    
        except:
            logging.error("Json Data Wasn't successfully read from the file")
            return;
    
    
