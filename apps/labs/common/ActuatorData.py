'''
    Model class to store the Actuator Data
'''


class ActuatorData:
     
    '''
        Constructor for the Actuator Class
    '''

    def __init__(self):
        self.command = ''
        self.value = 0.0
        self.name = ''

    '''
        Create Custom Actuator Data
    '''

    def createCustomActuatorData(self, command, value, name):
        self.command = command
        self.value = value
        self.name = name
        
    '''
        Fetches the command as a String
    '''

    def getCommand(self):
        return self.command
    
    '''
        Sets the value of the command
    '''

    def setCommand(self, command):
        self.command = command
    
    '''
        Fetches the name of the Sensor
    '''    

    def getName(self):
        return self.name
    
    '''
        Sets the name of the Sensor
    '''

    def setName(self, name='Temperature'):
        self.name = name
    
    '''
        Gets the current value of the Actuator
    '''    

    def getValue(self):
        return self.value
    
    '''
        Sets the current value of the Actuator
    '''   

    def setValue(self, val):
        self.value = val
