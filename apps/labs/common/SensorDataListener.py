from labs.common.SensorData import SensorData

import logging

logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', level=logging.DEBUG, datefmt='%m/%d/%Y %I:%M:%S %p')


class SensorDataListener:
    
    sensorData = SensorData()
    
    '''
        Callback function to get the sensorData from the channel subscribed
    '''

    def onMessage(self, sensorData):
        self.sensorData = sensorData
    
    '''
        Helper method to retrieve the sensorData supplied by Gateway App
    ''' 
    
    def getSensorData(self):
        return self.sensorData
