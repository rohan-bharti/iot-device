import logging
import redis

from labs.common.DataUtil import DataUtil
from labs.common.ConfigUtil import ConfigUtil

logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', level=logging.DEBUG, datefmt='%m/%d/%Y %I:%M:%S %p')


class PersistenceUtil:
    
    '''
        Constructor sets up the required instances
    '''

    def __init__(self):
        self.dataUtil = DataUtil()
        self.configUtil = ConfigUtil(None)
        self.redisPythonClient = None
        self.actuatorPubsub = None
        self.sensorPubsub = None
        self.setupRedis()
    
    '''
        Registers the actuator listener
    '''

    def registerActuatorDataDbmsListener(self, callback):
        self.actuatorPubsub.subscribe('actuatorDataGatewayChannel')
        if self.getDataFromChannel(self.actuatorPubsub, callback):
            self.actuatorPubsub.unsubscribe('actuatorDataGatewayChannel')
            return True
        
    '''
        Writes the sensor Data to the DB and publishes SensorData on its channel
    '''      

    def writeSensorDataToDbms(self, sensorData):
        sensorDataKey = "sensorData" + sensorData.getTimeStamp()
        sensorDataStr = self.dataUtil.toJsonFromSensorData(sensorData)
        logging.info('The SensorData Json is: ' + sensorDataStr)
        try:
            self.redisPythonClient.set(sensorDataKey, sensorDataStr)
            # Publishing The SensorData
            self.redisPythonClient.publish("sensorDataDeviceChannel", sensorDataStr)
            logging.info("Json Sensor Data published on sensorDataDeviceChannel and written successfully to the Redis Database")
        except:
            logging.error('Json Sensor Data WAS NOT written to the Database neither published')
    
    '''
        Registers the sensor listener
    '''     

    def registerSensorDataDbmsListener(self, callback=None):
        # Not Being Used as of now
        self.sensorPubsub.subscribe('sensorDataGatewayChannel')
        if self.getDataFromChannel(self.sensorPubsub, callback):
            return True
    
    '''
        Writes the actuator Data to the DB and publishes ActuatorData on its channel
    '''     

    def writeActuatorDataToDbms(self, actuatorData):
        actuatorDataKey = "actuatorData"
        actuatorDataStr = self.dataUtil.toJsonFromActuatorData(actuatorData)
        logging.info('The ActuatorData Json is: ' + actuatorDataStr)
        try:
            self.redisPythonClient.set(actuatorDataKey, actuatorDataStr)
            # Publishing The ActuatorData
            # Not Being Used as of now
            self.redisPythonClient.publish("actuatorDataDeviceChannel", actuatorDataStr)
            logging.info("Json Actuator Data published on actuatorDataDeviceChannel and written successfully to the Redis Database")
        except:
            logging.error('Json Actuator Data WAS NOT written to the Database')
     
    '''
        A helper method to get data from the channel of the supplied pubsub object
    '''     

    def getDataFromChannel(self, pubsub, callback):
        flag = True
        try:
            while flag:
                actuatorData = pubsub.get_message()
                if actuatorData != None:
                    actuatorDataStr = actuatorData.get('data').decode()
                    logging.info("Actuator Data received from Java Gateway: " + actuatorDataStr)
                    actuatorData = self.dataUtil.toActuatorDataFromJson(actuatorDataStr)
                    callback(actuatorData)
                    flag = False
                    return True
        except Exception as e:
            logging.error("Data wasn't received successfully from the channel")
    
    '''
        Fetches the configuration data for redis using ConfigUtil and sets up an instance for Redis
    ''' 

    def setupRedis(self):
        try:
            hostName = self.configUtil.getValue('redis', 'host')
            portNum = self.configUtil.getIntegerValue('redis', 'port')
            redisPassword = self.configUtil.getValue('redis', 'password')
            logging.info('Redis Configuration details fetched successfully!')
            self.redisPythonClient = redis.StrictRedis(host=hostName, port=portNum, db=0, password=redisPassword)
            self.actuatorPubsub = self.redisPythonClient.pubsub(ignore_subscribe_messages=True)
            self.sensorPubsub = self.redisPythonClient.pubsub(ignore_subscribe_messages=True)
            logging.info('Redis Setup successfully at host: ' + hostName + ' and Port: ' + str(portNum))
        except Exception as e:
            logging.error(e)
            logging.error('Redis was not setup successfully on the Python Device app')
        
