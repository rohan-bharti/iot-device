from labs.module03.TempSensorAdaptorTask import TempSensorAdaptorTask

import logging

'''
    Instantiates the run method of TempSensorAdaptorTask Thread
'''


class TempSensorAdaptor:
    
    logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', level=logging.DEBUG, datefmt='%m/%d/%Y %I:%M:%S %p')

    '''
        Starts the temperature polling
    '''

    def startReadingTemperature(self, sensorDataManager):
        tempSensorAdaptorTask = TempSensorAdaptorTask(sensorDataManager)
        logging.info('Started Polling temperature')
        tempSensorAdaptorTask.start()
    
