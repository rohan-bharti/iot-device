from sense_hat import SenseHat

'''
    SenseHat class which helps with displaying the colors on the SenseHat 
'''


class SenseHatLedActivator:
    
    '''
        Changes the color based on the command supplied by the ActuatorData
    '''       

    def changeColor(self, color):
        sense = SenseHat()
        
        redColor = (255, 0, 0)
        blueColor = (0, 0, 255)
        greenColor = (0, 255, 0)
        
        if color == 'RED':
            sense.clear(redColor)
            
        elif color == 'BLUE':
            sense.clear(blueColor)
            
        elif color == 'GREEN':
            sense.clear(greenColor)
        
