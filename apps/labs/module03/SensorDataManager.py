from labs.common.ActuatorData import ActuatorData

from labs.module03.TempSensorAdaptor import TempSensorAdaptor
from labs.module02.SmtpClientConnector import SmtpClientConnector
from labs.module03.TempActuatorAdaptor import TempActuatorAdaptor
from labs.common.ConfigUtil import ConfigUtil

import logging

'''
    SensorDataManager - Handles emailing the user, initializing the ConfigUtil, checking for actuation and supplying relevant 
    ActuatorData
'''


class SensorDataManager:
    
    logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', level=logging.DEBUG, datefmt='%m/%d/%Y %I:%M:%S %p')
    
    '''
        Constructor to initialize all the dependencies
    '''

    def __init__(self):
        self.actuatorData = ActuatorData()
        self.tempSensorAdaptor = TempSensorAdaptor()
        self.tempActuatorAdaptor = TempActuatorAdaptor()
        self.connector = SmtpClientConnector()
        self.configUtil = ConfigUtil(None)
        self.safeValue = 10.0
    
    '''
        Starts the temperature Polling process
    '''

    def kickOffUpdatingSensorData(self):
        self.actuatorData.setName('Temperature')
        logging.info('SensorDataManager started talking to the TempSensorAdaptor class')
        self.tempSensorAdaptor.startReadingTemperature(self)
    
    '''
        Gets the nominal temperature from the Config File
    '''

    def getNominalTempValue(self):
        nominalTemp = self.configUtil.getIntegerValue('device', 'nominalTemp')
        if nominalTemp == 'INTEGER NOT FOUND':
            logging.error('NOMINAL TEMPERATURE WAS NOT FOUND IN THE CONFIGURATION FILE!')
        return nominalTemp
    
    '''
        Fetches the nominal temperature, compares it with the current temperature reading supplied by the SenseHat, if it
        deviates by a margin of 3, it updates the actuator Data and on the basis of that does the necessary actuation by calling
        the updateActuator() method of TempActuatorAdaptor 
    '''

    def handleSensorData(self, sensorData):
        nominalTempValue = self.getNominalTempValue()
        curValue = sensorData.getCurrentValue()
        
        if (curValue - nominalTempValue) > self.safeValue:
            logging.info('Temperature recorded as {TOO HOT}')
            self.actuatorData.setCommand('TOO HOT')
            self.actuatorData.setValue(curValue)
            self.connector.serverSetupAndSendMessage(sensorData)
            return self.tempActuatorAdaptor.updateActuator(self.actuatorData)
            
        elif (curValue - nominalTempValue) < (-1 * self.safeValue):
            logging.info('Temperature recorded as {TOO COLD}')
            self.actuatorData.setCommand('TOO COLD')
            self.actuatorData.setValue(curValue)
            self.tempActuatorAdaptor.updateActuator(self.actuatorData)
            return self.tempActuatorAdaptor.updateActuator(self.actuatorData)
            
        else:
            logging.info('Temperature recorded as {NORMAL}')
            self.actuatorData.setCommand('NORMAL')
            self.actuatorData.setValue(curValue)
            return self.tempActuatorAdaptor.updateActuator(self.actuatorData)
        
        
