from labs.module03.SenseHatLedActivator import SenseHatLedActivator

import logging

'''
    Interacts with the SenseHatLed Class on the basis of the command supplied by the ActuatorData
'''


class TempActuatorAdaptor:
    
    '''
        Calls the actuation color change method on the basis of the command supplied
    ''' 

    def updateActuator(self, actuatorData):
        senseHat = SenseHatLedActivator()
        
        if actuatorData.getCommand() == 'TOO HOT':
            logging.info('Firing off Red Color on the SenseHat')
            senseHat.changeColor('RED')
            return True
        
        elif actuatorData.getCommand() == 'TOO COLD':
            logging.info('Firing off Blue Color on the SenseHat')
            senseHat.changeColor('BLUE')
            return True
            
        elif actuatorData.getCommand() == 'NORMAL':
            logging.info('Firing off Green Color on the SenseHat')
            senseHat.changeColor('GREEN')
            return True
        
        else:
            return False
        
