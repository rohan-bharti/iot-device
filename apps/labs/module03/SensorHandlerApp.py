from labs.module03.SensorDataManager import SensorDataManager

'''
    Initializes SensorDataManager and kicks off the application
'''
sensorDataManager = SensorDataManager()
sensorDataManager.kickOffUpdatingSensorData()
