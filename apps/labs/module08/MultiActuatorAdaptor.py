from labs.module08.SenseHatLedActivator import SenseHatLedActivator

import logging

'''
    MultiActuatorAdaptor to set the MqttClient
'''

logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', level=logging.DEBUG, datefmt='%m/%d/%Y %I:%M:%S %p')

class MultiActuatorAdaptor:
    
    def updateActuator(self, actuatorData):
        senseHat = SenseHatLedActivator()
        
        # Actuation for Temperature Change
        if actuatorData.getName() == 'Temperature':
            if actuatorData.getCommand() == 'UBIDOTS_TEMP_ACTUATOR':
                logging.info('Firing off Notification from MQTT Gateway')
                senseHat.showMessage('UBIDOTS', str(round(actuatorData.getValue(), 2)))
                return True