import paho.mqtt.client as mqtt
from urllib.parse import urlparse
import os
import logging
from labs.common.DataUtil import DataUtil
from labs.common.ActuatorDataListener import ActuatorDataListener
from labs.common.SensorDataListener import SensorDataListener
from labs.module08.MultiActuatorAdaptor import MultiActuatorAdaptor

logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', level=logging.DEBUG, datefmt='%m/%d/%Y %I:%M:%S %p')

# Constants
mqttBrokerAddr = 'tcp://mqtt.eclipse.org:1883'
sensorDataTopic = 'topic/rohan/test'
actuatorDataTopic = 'topic/rohan/actuatorData'


# Define event callbacks
def on_connect(client, userdata, flags, rc):
    logging.info("Connected successfully to the broker")


def on_message(client, userdata, message):
    MqttClientConnector().messageReceived(message)


def on_publish(client, obj, mid):
    logging.info("Published to the broker successfully")


def on_subscribe(client, obj, mid, granted_qos):
    logging.info("Subscribed: " + str(mid) + " " + str(granted_qos))

'''
    MqttClientConnector. Defines all the functionalities required by the MQTT Client.
'''


class MqttClientConnector:
    
    '''
        Constructor
    '''

    def __init__(self):
        self.mqttClient = mqtt.Client()
        self.setupMqttClient()
        self.mqttClient.on_message = on_message
        self.mqttClient.on_connect = on_connect
        self.mqttClient.on_publish = on_publish
        self.mqttClient.on_subscribe = on_subscribe
        self.dataUtil = DataUtil()
        self.actuatorDataListener = None
        self.sensorDataListener = None
        self.multiActuatorAdaptor = MultiActuatorAdaptor();
    
    '''
        Sets up the configuration for the MqttClient
    '''

    def setupMqttClient(self):
        url_str = os.environ.get('CLOUDMQTT_URL', mqttBrokerAddr)
        url = urlparse(url_str)
        self.mqttClient.connect(url.hostname, url.port)
    
    '''
        Publishes ActuatorData
    '''

    def publishActuatorCommand(self, actuatorData, qos):
        actuatorDataJson = self.dataUtil.toJsonFromActuatorData(actuatorData)
        logging.info("JSON BEFORE: " + actuatorDataJson)
        self.mqttClient.publish(actuatorDataTopic, actuatorDataJson, qos)
        return True
    
    '''
        Publishes SensorData
    '''   

    def publishSensorData(self, sensorData, qos):
        sensorDataJson = self.dataUtil.toJsonFromSensorData(sensorData)
        logging.info("JSON BEFORE: " + sensorDataJson)
        self.mqttClient.publish(sensorDataTopic, sensorDataJson, qos)
        return True
    
    '''
        Registers ActuatorData Listener
    '''   

    def registerActuatorDataListener(self):
        self.actuatorDataListener = ActuatorDataListener()
    
    '''
        Registers SensorDataListener
    '''   

    def registerSensorDataListener(self):
        self.sensorDataListener = SensorDataListener()
    
    '''
        Subscribes to the ActuatorCommands
    '''   

    def subscribeToActuatorCommands(self, qos):
        logging.info("Subscribing to :" + actuatorDataTopic)
        self.mqttClient.subscribe(actuatorDataTopic, qos)
    
    '''
        Subscribes to the SensorData
    '''

    def subscribeToSensorData(self, qos):
        self.mqttClient.subscribe(sensorDataTopic, qos)
    
    '''
        Invoked when a new message arrives on the respective channel and does further processing
    '''  

    def messageReceived(self, mqttMessage):
        self.registerActuatorDataListener()
        self.registerSensorDataListener()
        if mqttMessage.topic == sensorDataTopic:
            sensorDataJson = str(mqttMessage.payload.decode("utf-8"))
            logging.info("JSON AFTER: " + sensorDataJson)
            sensorData = self.dataUtil.toSensorDataFromJson(sensorDataJson)
            sensorDataJsonAfterProcessing = self.dataUtil.toJsonFromSensorData(sensorData)
            logging.info("JSON AFTER PROCESSING: " + sensorDataJsonAfterProcessing)
            self.sensorDataListener.onMessage(sensorData)
            return True
        elif mqttMessage.topic == actuatorDataTopic:
            actuatorDataJson = str(mqttMessage.payload.decode("utf-8"))
            logging.info("JSON AFTER: " + actuatorDataJson)
            actuatorData = self.dataUtil.toActuatorDataFromJson(actuatorDataJson)
            actuatorDataJsonAfterProcessing = self.dataUtil.toJsonFromActuatorData(actuatorData)
            logging.info("JSON AFTER PROCESSING: " + actuatorDataJsonAfterProcessing)
            self.actuatorDataListener.onMessage(actuatorData)
            self.multiActuatorAdaptor.updateActuator(actuatorData)
            return True
        else:
            logging.info(mqttMessage.topic + " " + str(mqttMessage.qos) + " " + str(mqttMessage.payload))
            return False
    
    '''
        Continuously listening
    '''       

    def start(self):
        rc = 0
        while rc == 0:
            rc = self.mqttClient.loop()
            
        logging.info("rc: " + str(rc))
        
    
