from sense_hat import SenseHat

import time

'''
    SenseHat class which helps with displaying the colors on the SenseHat 
'''

# Color Codes R,G,B Values
redColor = [255, 0, 0]
blueColor = [0, 0, 255]
greenColor = [0, 255, 0]


class SenseHatLedActivator:
    
    def __init__(self):
        self.sense = SenseHat()
    
            
    '''
        Shows the message based on the text supplied on the SenseHat
    ''' 
            
    def showMessage(self, command, value):

        message = "{}: {}".format(command, value)
        
        # SenseHat notif will be in White with Red Color Background
        if command == 'UBIDOTS':
            self.sense.show_message(message, back_colour=redColor)
            time.sleep(5)
        
