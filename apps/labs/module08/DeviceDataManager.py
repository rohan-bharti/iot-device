import logging

from labs.module08.MqttClientConnector import MqttClientConnector

logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', level=logging.DEBUG, datefmt='%m/%d/%Y %I:%M:%S %p')

'''
    DataManager class for Module 06, generates dummy SensorData and publishes it on the MQTT broker.
'''


class DeviceDataManager:
    
    sensorData = None
    
    '''
        Constructor
    '''

    def __init__(self):
        self.mqttClient = None
    
     
    '''
        Sets up the MQTT Client
    '''    

    def setupMqttClient(self):
        self.mqttClient = MqttClientConnector()

    '''
        Starts listening for the ActuatorData on the channel on MQTT Broker
    '''  

    def subscribeToActuatorDataFromGateway(self):
        self.setupMqttClient()
        self.mqttClient.subscribeToActuatorCommands(1)
        self.mqttClient.start()
