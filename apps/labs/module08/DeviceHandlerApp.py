from labs.module08.DeviceDataManager import DeviceDataManager

'''
    Script to start the application
'''
deviceDataManager = DeviceDataManager()

deviceDataManager.subscribeToActuatorDataFromGateway()
