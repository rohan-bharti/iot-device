import logging
import schedule 

from random import choice
from labs.common.SensorData import SensorData
from labs.module02.SmtpClientConnector import SmtpClientConnector

class TempSensorEmulatorTask:
    
    logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', level=logging.DEBUG, datefmt='%m/%d/%Y %I:%M:%S %p')
        
    '''
        Initializes SensorData, Current Value and Alerting Difference (by default 10) as supplied.
    '''
    def __init__(self, alertDiff=10):
        self.sensorData = SensorData()
        self.connector = SmtpClientConnector()
        self.curVal = 0.0
        self.alertDiff = alertDiff
        
    '''
        Thread's run method for the Random Temperature Value Generation every 10 to 20 seconds.
    '''    
    def scheduleEmulatorTask(self):
        schedule.every(10).to(60).seconds.do(self.generateRandomTemperatureValue)
        while True:
            schedule.run_pending()    
        
    '''
        Generates a Random Temperature value between 0 and 30,
    '''
    def generateRandomTemperatureValue(self):
        randomList = [i for i in range(31)]
        self.curVal = choice(randomList)
        logging.info("The current Temperature reading: " + str(self.curVal))
        self.sensorData.addValue(self.curVal)
        self.sendNotification()    
    
    '''
        Checks if the new value generated is greater than the average value by the alertDiff configured in
        the constructor.
    '''
    def sendNotification(self):
        if (abs(self.curVal - self.sensorData.getAverageValue()) >= self.alertDiff):
            logging.info('\n Current temp exceeds average by > ' + str(self.alertDiff) + '. Triggering alert...')
            self.connector.serverSetupAndSendMessage(self.getSensorData())        
    
    '''
        Supplies the sensorData object.
    '''       
    def getSensorData(self):
        return self.sensorData

    