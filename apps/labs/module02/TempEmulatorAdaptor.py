from labs.module02.TempSensorEmulatorTask import TempSensorEmulatorTask

'''
    Instantiates the TempSensorEmulatorTask thread.
'''
class TempEmulatorAdaptor():
    
    def __init__(self):
        self.tempEmulator = TempSensorEmulatorTask()
        
    '''
        Starts the TempSensorEmulatorTask thread.
    '''
    def startExecution(self):
        self.tempEmulator.scheduleEmulatorTask()

tempEmulator = TempEmulatorAdaptor();
tempEmulator.startExecution();