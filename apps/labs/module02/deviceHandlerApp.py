import logging

from labs.module02.TempEmulatorAdaptor import TempEmulatorAdaptor

logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', level=logging.DEBUG, datefmt='%m/%d/%Y %I:%M:%S %p')

'''
    Starts the process of monitoring the sensor Data generated periodically by the emulator and once crosses the Threshold,
    sends off an email to the user
'''
def runTemperatureMonitoring():
    tempEmulator = TempEmulatorAdaptor();
    tempEmulator.startExecution();
    
runTemperatureMonitoring()