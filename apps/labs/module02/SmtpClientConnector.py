import logging
import smtplib

from labs.common.ConfigUtil import ConfigUtil

from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

'''
    SmtpClientConnector class, sets up the server and sends an email as per the configuration file.
'''
class SmtpClientConnector:
    
    logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', level=logging.DEBUG, datefmt='%m/%d/%Y %I:%M:%S %p')
    
    def __init__(self):
        self.conifg = ConfigUtil(None)
        self.section = 'smtp.cloud'
        self.smtpServer = self.conifg.getValue(self.section, 'host')
        self.smtpPort = self.conifg.getIntegerValue(self.section, 'port')
        self.senderEmail = self.conifg.getValue(self.section, 'fromAddr')
        self.receiverEmail = self.conifg.getValue(self.section, 'toAddr')
        self.emailAppPassword = self.conifg.getValue(self.section, 'authToken')
        
    '''
        Sets up the server and sends an email with the SensorData information.
    '''    
    def serverSetupAndSendMessage(self, sensorData):
        try:
            server = smtplib.SMTP_SSL(self.smtpServer, self.smtpPort)
            server.login(self.senderEmail, self.emailAppPassword)
            message = self.createMessage(sensorData)
            server.sendmail(self.senderEmail, self.receiverEmail, message.as_string())
            logging.info("Email Sent")
        except Exception as e:
            logging.error("The server wasn't setup successfully: " + str(e))
    
    '''
        Creates a well formed message.
    '''
    def createMessage(self, sensorData):
        message = MIMEMultipart("alternative")
        message["Subject"] = "{} Update".format(sensorData.getName())
        message["From"] = self.senderEmail
        message["To"] = self.receiverEmail
        msgBodyText = """
        {}:
            Time:     {}
            Current:  {}
            Average:  {}
            Samples:  {}
            Min:      {}
            Max:      {}
         """.format(sensorData.getName(),sensorData.getTimeStamp(), str(sensorData.getCurrentValue()), str(sensorData.getAverageValue())
                           , str(sensorData.getCount()), str(sensorData.getMinValue()), str(sensorData.getMaxValue()))
        msgBody = MIMEText(msgBodyText, "plain")
        message.attach(msgBody)
        logging.info(msgBodyText)
        return message
    