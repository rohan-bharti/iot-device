from threading import Thread
from sense_hat import SenseHat
from random import randint

import logging
import time

from labs.common.SensorData import SensorData

'''
    Makes use of the Sense Hat Library to get the temperature
'''


class TempSensorAdaptorTask(Thread):
    
    logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', level=logging.DEBUG, datefmt='%m/%d/%Y %I:%M:%S %p')
    
    '''
        Constructor - Initializes SensorData and SenseHat
    '''

    def __init__(self):
        Thread.__init__(self)
        self.sensorData = SensorData()
        self.sensorData.setName('Temperature')
        self.senseHat = SenseHat()
    
    '''
        Fetches the temperature from the SenseHat 
    '''

    def updateSensorData(self):
        temp = self.senseHat.get_temperature()
        self.sensorData.addValue(temp) 
        
    '''
        Runs continuously and updates the SensorData with the latest Temperature reading supplied by the SenseHat
    '''

    def run(self):
        while True:
            secondsToSleep = randint(60, 70)
            time.sleep(secondsToSleep)
            self.updateSensorData()
            logging.info('Updated the SensorData Object, Temp: ' + str(self.sensorData.getCurrentValue()))
    
    '''
        Supplies the Current SensorData Object
    '''       
    def getSensorData(self):
        return self.sensorData
        
