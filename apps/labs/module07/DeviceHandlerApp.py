from labs.module07.DeviceDataManager import DeviceDataManager

'''
    Script to start the application
'''

deviceDataManager = DeviceDataManager()

deviceDataManager.makePostRequest()
deviceDataManager.makeGetRequest()
deviceDataManager.makePutrequest()
deviceDataManager.makeDeleteRequest()