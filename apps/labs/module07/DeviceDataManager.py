import logging

from labs.module07.TempSensorAdaptorTask import TempSensorAdaptorTask
from labs.module07.MultiSensorAdaptor import MultiSensorAdaptor

logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', level=logging.DEBUG, datefmt='%m/%d/%Y %I:%M:%S %p')

'''
    DataManager class for Module 06, generates dummy SensorData and publishes it on the MQTT broker.
'''

class DeviceDataManager:
    
    sensorData = None
    
    '''
        Constructor
    '''
    
    def __init__(self):
        self.tempAdaptorTask = TempSensorAdaptorTask()
        self.coapClient = MultiSensorAdaptor.setupCoapClient(self)
        
    '''
        Creates a SensorData Object, with information being supplied from the SenseHat
    '''
        
    def getSampleSensorData(self):
        self.tempAdaptorTask.updateSensorData()
        sensorData = self.tempAdaptorTask.getSensorData()
        return sensorData
    
    '''
        Make a GET Request
    '''
    
    def makeGetRequest(self):
        self.coapClient.getSensorData()
        
    '''
        Make a POST Request
    '''
        
    def makePostRequest(self):
        sensorData = self.getSampleSensorData()
        self.coapClient.sendSensorData(sensorData)
    
    '''
        Make a PUT Request
    '''   
        
    def makePutrequest(self):
        self.coapClient.updateSensorData()
        
    '''
        Make a DELETE Request
    ''' 
        
    def makeDeleteRequest(self):
        self.coapClient.deleteSensorData()
