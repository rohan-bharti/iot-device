from labs.module07.CoapClientConnector import CoapClientConnector

'''
    Returns an instance of the CoapClientConnector
'''

class MultiSensorAdaptor:
    
    def setupCoapClient(self):
        coapClient = CoapClientConnector()
        return coapClient
        