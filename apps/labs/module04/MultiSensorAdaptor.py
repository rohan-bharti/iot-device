from labs.module04.TempSensorAdaptorTask import TempSensorAdaptorTask
from labs.module04.HI2CSensorAdaptorTask import HI2CSensorAdaptorTask
from labs.module04.HumiditySensorAdaptorTask import HumiditySensorAdaptorTask

import logging

'''
    Instantiates the run method of TempSensorAdaptorTask Thread
'''


class MultiSensorAdaptor:
    
    logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', level=logging.DEBUG, datefmt='%m/%d/%Y %I:%M:%S %p')

    '''
        Starts the temperature polling
    '''

    def startReadingTemperature(self, sensorDataManager):
        tempSensorAdaptorTask = TempSensorAdaptorTask(sensorDataManager)
        logging.info('Started Polling temperature')
        tempSensorAdaptorTask.start()
        
    '''
        Starts the Humidity polling from SenseHat
    '''

    def startReadingHumiditySenseHat(self, sensorDataManager):
        humiditySensorAdaptorTask = HumiditySensorAdaptorTask(sensorDataManager)
        logging.info('Started Polling humidity from SenseHat')
        humiditySensorAdaptorTask.start()   
        
    '''
        Starts the Humidity polling from I2C Bus (SMBus Library)
    '''

    def startReadingHumidityI2CBus(self, sensorDataManager):
        hi2cSensorAdaptorTask = HI2CSensorAdaptorTask(sensorDataManager)
        logging.info('Started Polling humidity from I2C Bus')
        hi2cSensorAdaptorTask.start()
