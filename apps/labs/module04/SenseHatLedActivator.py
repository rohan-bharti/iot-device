from sense_hat import SenseHat

import time

'''
    SenseHat class which helps with displaying the colors on the SenseHat 
'''

# Color Codes R,G,B Values
redColor = [255, 0, 0]
blueColor = [0, 0, 255]
greenColor = [0, 255, 0]


class SenseHatLedActivator:
    
    def __init__(self):
        self.sense = SenseHat()
    
    '''
        Changes the color based on the command supplied by the ActuatorData
    '''       

    def changeColor(self, color):
        
        if color == 'RED':
            self.sense.clear(redColor)
            time.sleep(5)
            
        elif color == 'BLUE':
            self.sense.clear(blueColor)
            time.sleep(5)
            
        elif color == 'GREEN':
            self.sense.clear(greenColor)
            time.sleep(5)
            
    '''
        Shows the message based on the text supplied on the SenseHat
    ''' 
            
    def showMessage(self, command, value):

        message = "{}: {}".format(command, value)
        
        # SenseHat notif will be in White with Red Color Background
        if command == 'SENSEHAT':
            self.sense.show_message(message, back_colour=redColor)
            time.sleep(8)
        # I2C notif will be in White Color with Blue Color Background
        elif command == 'I2C':
            self.sense.show_message(message, back_colour=blueColor)
            time.sleep(8)
        
