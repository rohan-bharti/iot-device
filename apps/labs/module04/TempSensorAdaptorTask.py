from threading import Thread
from sense_hat import SenseHat
from random import randint

import logging
import time

from labs.common.SensorData import SensorData

'''
    Makes use of the Sense Hat Library to get the temperature
'''


class TempSensorAdaptorTask(Thread):
    
    logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', level=logging.DEBUG, datefmt='%m/%d/%Y %I:%M:%S %p')
    
    '''
        Constructor - Initializes SensorData and SenseHat
    '''

    def __init__(self, sensorDataManager):
        Thread.__init__(self)
        self.sensorData = SensorData()
        self.sensorData.setName('Temperature')
        self.senseHat = SenseHat()
        self.sensorDataManager = sensorDataManager
    
    '''
        Fetches the temperature from the SenseHat 
    '''

    def updateSensorData(self):
        temp = self.senseHat.get_temperature()
        self.sensorData.addValue(temp) 
        
    '''
        Runs continuously and updates the SensorData with the latest Temperature reading supplied by the SenseHat
    '''

    def run(self):
        while True:
            self.updateSensorData()
            logging.info('Updated the SensorData Object, Temp: ' + str(self.sensorData.getCurrentValue()))
            self.sensorDataManager.handleSensorData(self.sensorData)
            secondsToSleep = randint(20, 60)
            time.sleep(secondsToSleep)
        
