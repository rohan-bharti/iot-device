from labs.module04.SenseHatLedActivator import SenseHatLedActivator

import logging

'''
    Interacts with the SenseHatLed Class on the basis of the command supplied by the ActuatorData
'''


class MultiActuatorAdaptor:
    
    '''
        Calls the actuation color change method on the basis of the command supplied
    ''' 

    def updateActuator(self, actuatorData):
        senseHat = SenseHatLedActivator()
        
        # Actuation for Temperature Change
        if actuatorData.getName() == 'Temperature':
            if actuatorData.getCommand() == 'TOO HOT':
                logging.info('Firing off Red Color on the SenseHat')
                senseHat.changeColor('RED')
                return True
            
            elif actuatorData.getCommand() == 'TOO COLD':
                logging.info('Firing off Blue Color on the SenseHat')
                senseHat.changeColor('BLUE')
                return True
                
            elif actuatorData.getCommand() == 'NORMAL':
                logging.info('Firing off Green Color on the SenseHat')
                senseHat.changeColor('GREEN')
                return True
            
            else:
                logging.error('No value for Temperature in ActuatorData')
                return False
            
        # Actuation for Humidity Notification from SenseHat
        if actuatorData.getName() == 'Humidity_SENSEHAT':
            if actuatorData.getCommand() == 'SENSEHAT NOTIF':
                logging.info('Firing off Humidity Value from SenseHat on the SenseHat')
                senseHat.showMessage('SENSEHAT', str(round(actuatorData.getValue(), 2)))
                return True
            
            else:
                logging.error('No value for SenseHat Humidity in ActuatorData')
                return False
            
        # Actuation for Humidity Notification from I2CBus
        if actuatorData.getName() == 'Humidity_I2C':
            if actuatorData.getCommand() == 'I2CBUS NOTIF':
                logging.info('Firing off Humidity Value from I2CBus on the SenseHat')
                senseHat.showMessage('I2C', str(round(actuatorData.getValue(), 2)))
                return True
            
            else:
                logging.error('No value for I2C Humidity in ActuatorData')
                return False
        
