from labs.common.ActuatorData import ActuatorData

from labs.module04.MultiSensorAdaptor import MultiSensorAdaptor
from labs.module02.SmtpClientConnector import SmtpClientConnector
from labs.module04.MultiActuatorAdaptor import MultiActuatorAdaptor
from labs.common.ConfigUtil import ConfigUtil
from threading import Lock

import logging

'''
    SensorDataManager - Handles emailing the user, initializing the ConfigUtil, checking for actuation and supplying relevant 
    ActuatorData
'''


class SensorDataManager:
    
    logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', level=logging.DEBUG, datefmt='%m/%d/%Y %I:%M:%S %p')
    
    '''
        Constructor to initialize all the dependencies
    '''

    def __init__(self):
        self.tempActuatorData = ActuatorData()
        self.humiditySenseHatActuatorData = ActuatorData()
        self.humidityI2CActuatorData = ActuatorData()
        self.multiSensorAdaptor = MultiSensorAdaptor()
        self.multiActuatorAdaptor = MultiActuatorAdaptor()
        self.connector = SmtpClientConnector()
        self.configUtil = ConfigUtil(None)
        self.lock = Lock()
        self.safeValue = 10.0
    
    '''
        Starts the temperature Polling process
    '''

    def kickOffUpdatingTempertureSensorData(self):
        self.tempActuatorData.setName('Temperature')
        logging.info('SensorDataManager started talking to the MultiSensorAdaptor class for temperature')
        self.multiSensorAdaptor.startReadingTemperature(self)
        
    '''
        Starts the Humidity Polling process from SenseHat
    '''

    def kickOffUpdatingHumiditySensorDataSenseHat(self):
        self.humiditySenseHatActuatorData.setName('Humidity_SENSEHAT')
        logging.info('SensorDataManager started talking to the MultiSensorAdaptor class for Humidity - SenseHat')
        self.multiSensorAdaptor.startReadingHumiditySenseHat(self)
        
    '''
        Starts the temperature Polling process
    '''

    def kickOffUpdatingHumiditySensorDataI2C(self):
        self.humidityI2CActuatorData.setName('Humidity_I2C')
        logging.info('SensorDataManager started talking to the MultiSensorAdaptor class for Humidity - I2C')
        self.multiSensorAdaptor.startReadingHumidityI2CBus(self)
    
    '''
        Gets the nominal temperature from the Config File
    '''

    def getNominalTempValue(self):
        nominalTemp = self.configUtil.getIntegerValue('device', 'nominalTemp')
        if nominalTemp == 'INTEGER NOT FOUND':
            logging.error('NOMINAL TEMPERATURE WAS NOT FOUND IN THE CONFIGURATION FILE!')
            return
        return nominalTemp
    
    '''
        Fetches the nominal temperature, compares it with the current temperature reading supplied by the SenseHat, if it
        deviates by a margin of 3, it updates the actuator Data and on the basis of that does the necessary actuation by calling
        the updateActuator() method of MultiActuatorAdaptor 
    '''

    def handleSensorData(self, sensorData):
        
        self.lock.acquire()
        
        # Check if the SensorData is for Temperature
        if sensorData.getName() == 'Temperature':
            nominalTempValue = self.getNominalTempValue()
            curValue = sensorData.getCurrentValue()
            
            if (curValue - nominalTempValue) > self.safeValue:
                logging.info('Temperature recorded as {TOO HOT} - LOCK ACQUIRED')
                self.tempActuatorData.setCommand('TOO HOT')
                self.tempActuatorData.setValue(curValue)
                self.connector.serverSetupAndSendMessage(sensorData)
                boolCheckHotActuation = self.multiActuatorAdaptor.updateActuator(self.tempActuatorData)
                self.lock.release()
                return boolCheckHotActuation
                
            elif (curValue - nominalTempValue) < (-1 * self.safeValue):
                logging.info('Temperature recorded as {TOO COLD} - LOCK ACQUIRED')
                self.tempActuatorData.setCommand('TOO COLD')
                self.tempActuatorData.setValue(curValue)
                self.connector.serverSetupAndSendMessage(sensorData)
                boolCheckColdActuation = self.multiActuatorAdaptor.updateActuator(self.tempActuatorData)
                self.lock.release()
                return boolCheckColdActuation
                
            else:
                logging.info('Temperature recorded as {NORMAL} - LOCK ACQUIRED')
                self.tempActuatorData.setCommand('NORMAL')
                self.tempActuatorData.setValue(curValue)
                boolCheckNormalActuation = self.multiActuatorAdaptor.updateActuator(self.tempActuatorData)
                self.lock.release()
                return boolCheckNormalActuation
            
        # Check if the SensorData is for Humidity from SenseHat
        if sensorData.getName() == 'Humidity_SENSEHAT':
            logging.info('Humidity recorded from SenseHat in the SensorDataManager - LOCK ACQUIRED')
            self.humiditySenseHatActuatorData.setCommand('SENSEHAT NOTIF')
            self.humiditySenseHatActuatorData.setValue(sensorData.getCurrentValue())
            self.connector.serverSetupAndSendMessage(sensorData)
            boolCheckSenseHatActuation = self.multiActuatorAdaptor.updateActuator(self.humiditySenseHatActuatorData)
            self.lock.release()
            return boolCheckSenseHatActuation
        
        # Check if the SensorData is for Humidity from I2C Bus
        if sensorData.getName() == 'Humidity_I2C':
            logging.info('Humidity recorded from I2CBus in the SensorDataManager - LOCK ACQUIRED')
            self.humidityI2CActuatorData.setCommand('I2CBUS NOTIF')
            self.humidityI2CActuatorData.setValue(sensorData.getCurrentValue())
            self.connector.serverSetupAndSendMessage(sensorData)
            boolCheckI2CActuation = self.multiActuatorAdaptor.updateActuator(self.humidityI2CActuatorData)
            self.lock.release()
            return boolCheckI2CActuation
        
        else:
            logging.error('The sensorData received is either null or of unknown type')
            self.lock.release()
            return False
        
