from labs.module04.SensorDataManager import SensorDataManager

'''
    Initializes SensorDataManager and kicks off the application to read temperature, Humidity from 
    SenseHat and Humidity from I2C Bus.
'''
sensorDataManager = SensorDataManager()

sensorDataManager.kickOffUpdatingTempertureSensorData()
sensorDataManager.kickOffUpdatingHumiditySensorDataSenseHat()
sensorDataManager.kickOffUpdatingHumiditySensorDataI2C()
