import logging

from labs.module06.TempSensorEmulator import TempSensorEmulator
from labs.module06.MultiSensorAdaptor import MultiSensorAdaptor
from labs.module06.MqttClientConnector import MqttClientConnector

logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', level=logging.DEBUG, datefmt='%m/%d/%Y %I:%M:%S %p')

'''
    DataManager class for Module 06, generates dummy SensorData and publishes it on the MQTT broker.
'''


class DeviceDataManager:
    
    sensorData = None
    
    '''
        Constructor
    '''

    def __init__(self):
        self.sensorDataEmulator = TempSensorEmulator()
        self.multiSensorAdaptor = MultiSensorAdaptor()
        self.mqttClient = None
    
    '''
        Constructs a dummy SensorData object
    '''

    def constructDummySensorData(self):
        self.sensorData = self.sensorDataEmulator.getSensorData()
        return self.sensorData
     
    '''
        Sets up the MQTT Client
    '''    

    def setupMqttClient(self):
        self.mqttClient = MqttClientConnector()
        self.multiSensorAdaptor.setMqttClient(self.mqttClient)

    '''
        Starts publishing the SensorData on the channel on MQTT Broker
    '''  

    def startPublishingSensorDataToMqttBroker(self):
        self.setupMqttClient()
        sensorData = self.constructDummySensorData()
        self.mqttClient.publishSensorData(sensorData, 2)
        self.mqttClient.start()
