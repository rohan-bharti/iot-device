import logging

from random import choice
from labs.common.SensorData import SensorData

'''
    Generates dummy SensorData for the SensorData test topic channel
'''


class TempSensorEmulator:
    
    logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', level=logging.DEBUG, datefmt='%m/%d/%Y %I:%M:%S %p')
        
    '''
        Initializes SensorData, Current Value and Alerting Difference (by default 10) as supplied.
    '''

    def __init__(self):
        self.sensorData = SensorData()
        self.curVal = 0.0  
        
    '''
        Generates a Random Temperature value between 0 and 30,
    '''

    def generateRandomTemperatureValue(self):
        self.sensorData.setName("Temperature")
        randomList = [i for i in range(31)]
        self.curVal = choice(randomList)
        logging.info("The current Temperature reading: " + str(self.curVal))
        self.sensorData.addValue(self.curVal)        
    
    '''
        Supplies the sensorData object.
    '''       

    def getSensorData(self):
        self.generateRandomTemperatureValue()
        return self.sensorData

    
