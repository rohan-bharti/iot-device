'''
    MultiSensorAdaptor to set up the MqttClient
'''

class MultiSensorAdaptor:
    
    mqttClient = None
    
    def setMqttClient(self, mqttClientConnector):
        self.mqttClient = mqttClientConnector