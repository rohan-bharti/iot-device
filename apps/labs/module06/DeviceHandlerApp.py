from labs.module06.DeviceDataManager import DeviceDataManager

'''
    Script to start the application
'''
deviceDataManager = DeviceDataManager()

deviceDataManager.startPublishingSensorDataToMqttBroker()
