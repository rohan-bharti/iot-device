from labs.common.ActuatorData import ActuatorData

from labs.module05.MultiSensorAdaptor import MultiSensorAdaptor
from labs.module05.MultiActuatorAdaptor import MultiActuatorAdaptor

import logging

'''
    SensorDataManager - Handles emailing the user, initializing the ConfigUtil, checking for actuation and supplying relevant 
    ActuatorData
'''


class DeviceDataManager:
    
    logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', level=logging.DEBUG, datefmt='%m/%d/%Y %I:%M:%S %p')
    
    '''
        Constructor to initialize all the dependencies
    '''

    def __init__(self):
        self.tempActuatorData = ActuatorData()
        self.humiditySenseHatActuatorData = ActuatorData()
        self.humidityI2CActuatorData = ActuatorData()
        self.multiSensorAdaptor = MultiSensorAdaptor()
        self.multiActuatorAdaptor = MultiActuatorAdaptor()

    '''
        Starts the temperature Polling process
    '''

    def kickOffUpdatingTempertureSensorData(self):
        self.tempActuatorData.setName('Temperature')
        logging.info('SensorDataManager started talking to the MultiSensorAdaptor class for temperature')
        self.multiSensorAdaptor.startReadingTemperature()
        
    '''
        Starts the Humidity Polling process from SenseHat
    '''

    def kickOffUpdatingHumiditySensorDataSenseHat(self):
        self.humiditySenseHatActuatorData.setName('Humidity_SENSEHAT')
        logging.info('SensorDataManager started talking to the MultiSensorAdaptor class for Humidity - SenseHat')
        self.multiSensorAdaptor.startReadingHumiditySenseHat()
        
    '''
        Starts the temperature Polling process
    '''

    def kickOffUpdatingHumiditySensorDataI2C(self):
        self.humidityI2CActuatorData.setName('Humidity_I2C')
        logging.info('SensorDataManager started talking to the MultiSensorAdaptor class for Humidity - I2C')
        self.multiSensorAdaptor.startReadingHumidityI2CBus()
        
    '''
        Starts listening for Actuation Data on the channel subscribed, from the Gateway Application
    '''
    
    def listenForActuation(self):
        logging.info('Started listening for Actuation Data from the Gateway Application')
        self.multiActuatorAdaptor.start()
          
