from labs.module05.TempSensorAdaptorTask import TempSensorAdaptorTask
from labs.module05.HI2CSensorAdaptorTask import HI2CSensorAdaptorTask
from labs.module05.HumiditySensorAdaptorTask import HumiditySensorAdaptorTask

import logging
from labs.common.PersistenceUtil import PersistenceUtil

'''
    Instantiates the run method of TempSensorAdaptorTask Thread
'''


class MultiSensorAdaptor:
    
    persistenceUtil = PersistenceUtil()
    
    logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', level=logging.DEBUG, datefmt='%m/%d/%Y %I:%M:%S %p')

    '''
        Starts the temperature polling
    '''

    def startReadingTemperature(self):
        tempSensorAdaptorTask = TempSensorAdaptorTask(self)
        logging.info('Started Polling temperature')
        tempSensorAdaptorTask.start()
        
    '''
        Starts the Humidity polling from SenseHat
    '''

    def startReadingHumiditySenseHat(self):
        humiditySensorAdaptorTask = HumiditySensorAdaptorTask(self)
        logging.info('Started Polling humidity from SenseHat')
        humiditySensorAdaptorTask.start()   
        
    '''
        Starts the Humidity polling from I2C Bus (SMBus Library)
    '''

    def startReadingHumidityI2CBus(self):
        hi2cSensorAdaptorTask = HI2CSensorAdaptorTask(self)
        logging.info('Started Polling humidity from I2C Bus')
        hi2cSensorAdaptorTask.start()
        
    '''
        Uses Persistence Util to write the updated sensorData to the Database
    '''
        
    def writeToDatabase(self, sensorData):
        self.persistenceUtil.writeSensorDataToDbms(sensorData)
