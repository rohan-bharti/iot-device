from labs.module05.SenseHatLedActivator import SenseHatLedActivator
from labs.common.ActuatorDataListener import ActuatorDataListener
from labs.common.PersistenceUtil import PersistenceUtil
from labs.common.ActuatorData import ActuatorData

from threading import Thread
import logging

logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', level=logging.DEBUG, datefmt='%m/%d/%Y %I:%M:%S %p')

'''
    Interacts with the SenseHatLed Class on the basis of the command supplied by the ActuatorData
'''

class MultiActuatorAdaptor(Thread):
    
    def __init__(self):
        Thread.__init__(self)
        self.actuatorDataListener = ActuatorDataListener()
        self.senseHat = SenseHatLedActivator()
        self.persistenceUtil = PersistenceUtil()
        self.senseHat = SenseHatLedActivator()
        
    '''
        Starts the thread and listens for ActuatorData update
    '''
        
    def run(self):
        while True:
            callback = self.actuatorDataListener.onMessage
            receiveData = self.persistenceUtil.registerActuatorDataDbmsListener(callback)
            if receiveData:
                logging.info('Received Actuation Data Information from the Gateway Java Application')
                actuatorData = self.actuatorDataListener.getActautorData()
                self.performActuation(actuatorData)
    
    '''
        Calls the actuation color change method on the basis of the command supplied
    ''' 
    
    def performActuation(self, actuatorData):
        # Actuation for Temperature Change
        if actuatorData.getName() == 'TEMP':
            if actuatorData.getCommand() == 'TOO HOT':
                logging.info('Firing off Red Color on the SenseHat')
                self.senseHat.changeColor('RED')
                return True
             
            elif actuatorData.getCommand() == 'TOO COLD':
                logging.info('Firing off Blue Color on the SenseHat')
                self.senseHat.changeColor('BLUE')
                return True
                 
            elif actuatorData.getCommand() == 'NORMAL':
                logging.info('Firing off Green Color on the SenseHat')
                self.senseHat.changeColor('GREEN')
                return True
             
            else:
                logging.error('No value for Temperature in ActuatorData')
                return False
             
        # Actuation for Humidity Notification from SenseHat
        if actuatorData.getName() == 'Humidity_SENSEHAT':
            if actuatorData.getCommand() == 'SENSEHAT NOTIF':
                logging.info('Firing off Humidity Value from SenseHat on the SenseHat')
                self.senseHat.showMessage('SENSEHAT', str(round(actuatorData.getValue(), 2)))
                return True
             
            else:
                logging.error('No value for SenseHat Humidity in ActuatorData')
                return False
             
        # Actuation for Humidity Notification from I2CBus
        if actuatorData.getName() == 'Humidity_I2C':
            if actuatorData.getCommand() == 'I2CBUS NOTIF':
                logging.info('Firing off Humidity Value from I2CBus on the SenseHat')
                self.senseHat.showMessage('I2C', str(round(actuatorData.getValue(), 2)))
                return True
             
            else:
                logging.error('No value for I2C Humidity in ActuatorData')
                return False
         

        
