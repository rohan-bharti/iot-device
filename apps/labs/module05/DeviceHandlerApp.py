from labs.module05.DeviceDataManager import DeviceDataManager

'''
    Initializes SensorDataManager and kicks off the application to read temperature, Humidity from 
    SenseHat and Humidity from I2C Bus.
'''
deviceDataManager = DeviceDataManager()

deviceDataManager.kickOffUpdatingTempertureSensorData()
deviceDataManager.kickOffUpdatingHumiditySensorDataSenseHat()
deviceDataManager.kickOffUpdatingHumiditySensorDataI2C()
deviceDataManager.listenForActuation()
