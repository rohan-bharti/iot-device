from threading import Thread
from sense_hat import SenseHat
from random import randint

import logging
import time

from labs.common.SensorData import SensorData

'''
    Makes use of the Sense Hat Library to get the humidity
'''


class HumiditySensorAdaptorTask(Thread):
    
    logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', level=logging.DEBUG, datefmt='%m/%d/%Y %I:%M:%S %p')
    
    '''
        Constructor - Initializes SensorData and SenseHat
    '''

    def __init__(self, multiSensorAdaptor):
        Thread.__init__(self)
        self.sensorData = SensorData()
        self.sensorData.setName('Humidity_SENSEHAT')
        self.senseHat = SenseHat()
        self.multiSensorAdaptor = multiSensorAdaptor
        
    '''
        Fetches the humidity from the SenseHat
    '''

    def getHumidity(self):
        return self.senseHat.get_humidity()
        
    '''
         Updates the SensorData with the received SenseHat Humidity Value
    '''

    def updateSensorData(self):
        humidity = self.getHumidity()
        self.sensorData.addValue(humidity) 
        
    '''
        Runs continuously and updates the SensorData with the latest Humidity reading supplied by the SenseHat
    '''

    def run(self):
        while True:
            secondsToSleep = randint(250,260)
            time.sleep(secondsToSleep)
            self.updateSensorData()
            logging.info('Updated the SensorData Object, Humidity from SENSEHAT: ' + str(self.sensorData.getCurrentValue()))
            self.multiSensorAdaptor.writeToDatabase(self.sensorData)
