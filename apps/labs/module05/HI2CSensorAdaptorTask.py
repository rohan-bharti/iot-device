from smbus2 import SMBus
from threading import Thread
from random import randint

import logging
import time

from labs.common.SensorData import SensorData

humAddr = 0x5F  # address for humidity sensor
HUMIDITY_OUT_L = 0x28  # address for humidity LSB
HUMIDITY_OUT_H = 0x29  # address for humidity MSB
H0_RH_X2 = 0x30
H1_RH_X2 = 0x31
H0_T0_OUT_L = 0x36
H0_T0_OUT_H = 0x37
H1_T0_OUT_L = 0x3A
H1_T0_OUT_H = 0x3B
totBytes = 6  # Total Bytes

'''
    Makes use of the SMBus (smbus2) Library to get the Humidity
'''


class HI2CSensorAdaptorTask(Thread):
    
    logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', level=logging.DEBUG, datefmt='%m/%d/%Y %I:%M:%S %p')
    
    '''
        Constructor - Initializes SensorData and SMBus
    '''

    def __init__(self, multiSensorAdaptor):
        Thread.__init__(self)
        self.sensorData = SensorData()
        self.sensorData.setName('Humidity_I2C')
        self.i2cBus = SMBus(1)
        self.address = humAddr
        self.multiSensorAdaptor = multiSensorAdaptor
        
    '''
        Fetches the Humidity from the I2CBus, uses registers defined initially to calculate the humidity value
    '''
        
    def getHumidity(self):
        h0_rh = self.i2cBus.read_byte_data(self.address, H0_RH_X2)
        h1_rh = self.i2cBus.read_byte_data(self.address, H1_RH_X2)
        
        H0_rH = h0_rh / 2
        H1_rh = h1_rh / 2
        
        h0_t0_l = self.i2cBus.read_byte_data(self.address, H0_T0_OUT_L)
        h0_t0_h = self.i2cBus.read_byte_data(self.address, H0_T0_OUT_H)
        H0_T0_OUT = self.convertData(h0_t0_l, h0_t0_h)  # int16
        
        h1_t0_l = self.i2cBus.read_byte_data(self.address, H1_T0_OUT_L)
        h1_t0_h = self.i2cBus.read_byte_data(self.address, H1_T0_OUT_H)
        H1_T0_OUT = self.convertData(h1_t0_l, h1_t0_h)  # int16
        
        h_out_l = self.i2cBus.read_byte_data(self.address, HUMIDITY_OUT_L)
        h_out_h = self.i2cBus.read_byte_data(self.address, HUMIDITY_OUT_H)
        H_T_OUT = self.convertData(h_out_l, h_out_h)
        
        humidity = H0_rH + (H1_rh - H0_rH) * (H_T_OUT - H0_T0_OUT) / (H1_T0_OUT - H0_T0_OUT)
        
        return humidity
    
    '''
        Takes data1, data2 (8bit) and returns value (16 bit)
    ''' 

    def convertData(self, data1, data2):
        value = data1 | (data2 << 8)
        if(value & (1 << 16 - 1)):
            value -= (1 << 16)
        return value
    
    '''
        Updates the SensorData with the Humidity supplied by the I2C Bus
    '''
        
    def updateSensorData(self):
        humidity = self.getHumidity()
        self.sensorData.addValue(humidity) 
        
    '''
        Runs continuously and updates the SensorData with the latest Humidity reading supplied by the I2CBus
    '''

    def run(self):
        while True:
            secondsToSleep = randint(150,180)
            time.sleep(secondsToSleep)
            self.updateSensorData()
            logging.info('Updated the SensorData Object, Humidity from I2CBus: ' + str(self.sensorData.getCurrentValue()))
            self.multiSensorAdaptor.writeToDatabase(self.sensorData)

