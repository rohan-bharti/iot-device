import schedule 
import time 
import logging

from labs.module01.systemPerformanceAdaptor import SystemPerformanceAdaptor

'''
    Python Script that initializes SystemPerformanceAdaptor and handles scheduling
'''

systemPerformanceAdaptor = SystemPerformanceAdaptor()
functionExecutionCount = 0
logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', level=logging.DEBUG, datefmt='%m/%d/%Y %I:%M:%S %p')

'''
    Keeps a track of the displayPerformanceInformation function calls
'''    
def incrementFunctionExecutionCount():
    global functionExecutionCount
    functionExecutionCount+=1
    
'''
    Displays the CPU Util and Memory Percentages
'''
def displayInformation():
    systemPerformanceAdaptor.displayPerformanceInformation()
    incrementFunctionExecutionCount()

'''
    Schedules to call displayInformation every 5 to 10 seconds 10 times  
'''  
def runApplication():
    flag = True
        
    schedule.every(5).to(10).seconds.do(displayInformation)
    
    while flag:
        schedule.run_pending() 
        time.sleep(1)
        if functionExecutionCount == 10:
            schedule.cancel_job(displayInformation)
            flag = False 
            logging.info("ALL PENDING INVOCATIONS COMPLETED!")
            
    
runApplication()